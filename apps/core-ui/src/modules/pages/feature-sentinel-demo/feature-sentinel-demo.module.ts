import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        loadChildren: () => import('./feature-sentinel-demo/feature-sentinel-demo.module')
          .then(m => m.FeatureSentinelDemoModule)
      },
      {
        path: 'with-else-template',
        loadChildren: () => import('./feature-sentinel-else-template-demo/feature-sentinel-else-template-demo.module')
          .then(m => m.FeatureSentinelElseTemplateDemoModule)
      },
    ])
  ]
})
export class FeatureSentinelDemoModule {

}
