import {ChangeDetectionStrategy, Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {
  StackDemoBasicUsageStackCodes
} from '@ui-demo/core/modules/pages/stack-demo/stack-demo-basic-usage/stack-demo-basic-usage-stack-codes';
import {Subscription} from 'rxjs';
import {IStackService, STACK_SERVICE} from '@ui-tool/core';
import {cloneDeep} from 'lodash-es';

@Component({
  selector: 'stack-demo-basic-usage',
  templateUrl: 'stack-demo-basic-usage.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StackDemoBasicUsageComponent implements OnInit, OnDestroy {

  private readonly __stackCodes: string[];
  private __codeToItemIds: Record<string, string[]>;
  private readonly __subscription: Subscription;

  public get stackCodes(): string[] {
    return this.__stackCodes;
  }

  public get codeToItemIds(): Record<string, string[]> {
    return this.__codeToItemIds;
  }

  public constructor(@Inject(STACK_SERVICE) private readonly __stackService: IStackService) {
    this.__stackCodes = Object.keys(StackDemoBasicUsageStackCodes);
    this.__codeToItemIds = {};
    this.__subscription = new Subscription();

    for (const stackCode of this.__stackCodes) {
      this.__codeToItemIds[stackCode] = [];
    }
  }

  public ngOnInit(): void {
    for (const stackCode of this.__stackCodes) {
      const hookStackEventSubscription = this.__stackService
        .hookStackEvent(stackCode)
        .subscribe(event => {
          const codeToItemIds = cloneDeep(this.__codeToItemIds);
          codeToItemIds[event.stackCode] = event.itemIds;
          this.__codeToItemIds = codeToItemIds;
        });
      this.__subscription.add(hookStackEventSubscription);
    }
  }

  public ngOnDestroy(): void {
    this.__subscription.unsubscribe();
  }

  public addToStack(stackCode: string): void {
    this.__stackService.push(stackCode);
  }

  public deleteFromStack(stackCode: string, itemId: string): void {
    this.__stackService.delete(stackCode, itemId);
  }

  public purge(stackCode: string): void {
    this.__stackService.purge(stackCode);
  }
}
