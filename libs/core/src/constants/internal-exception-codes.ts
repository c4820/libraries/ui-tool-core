// Raise the issue when no suitable validation item template builder is found.
// tslint:disable-next-line:max-line-length
export const NO_SUITABLE_VALIDATION_SUMMARIZER_ITEM_TEMPLATE_BUILDER_FOUND = 'NO_SUITABLE_VALIDATION_SUMMARIZER_ITEM_TEMPLATE_BUILDER_FOUND';
