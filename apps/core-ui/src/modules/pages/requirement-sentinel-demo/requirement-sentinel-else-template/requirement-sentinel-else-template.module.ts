import {NgModule} from '@angular/core';
import {RequirementSentinelElseTemplateComponent} from './requirement-sentinel-else-template.component';
import {RequirementSentinelElseTemplateRoutingModule} from './requirement-sentinel-else-template-routing.module';
import {REQUIREMENT_HANDLER, RequirementSentinelModule} from '@ui-tool/core';
import {ReactiveFormsModule} from '@angular/forms';
import {
  RequirementSentinelElseTemplateRequirementHandler
} from '@ui-demo/core/modules/pages/requirement-sentinel-demo/requirement-sentinel-else-template/requirement-sentinel-else-template.requirement-handler';

@NgModule({
  imports: [
    RequirementSentinelModule.forRoot(),
    RequirementSentinelElseTemplateRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [
    RequirementSentinelElseTemplateComponent
  ],
  providers: [
    {
      provide: REQUIREMENT_HANDLER,
      useClass: RequirementSentinelElseTemplateRequirementHandler,
      multi: true
    }
  ]
})
export class RequirementSentinelElseTemplateModule {

}
