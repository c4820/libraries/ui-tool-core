import {NgModule} from '@angular/core';
import {RequirementSentinelWithLogComponent} from './requirement-sentinel-with-log.component';
import {RequirementSentinelWithLogRoutingModule} from './requirement-sentinel-with-log-routing.module';
import {REQUIREMENT_HANDLER, REQUIREMENT_SENTINEL_EXCEPTION_PROCESSOR, RequirementSentinelModule} from '@ui-tool/core';
import {ReactiveFormsModule} from '@angular/forms';
import {
  RequirementSentinelWithLogRequirementHandler
} from '@ui-demo/core/modules/pages/requirement-sentinel-demo/requirement-sentinel-with-log/requirement-sentinel-with-log.requirement-handler';
import {
  RequirementSentinelWithLogExceptionProcessor
} from '@ui-demo/core/modules/pages/requirement-sentinel-demo/requirement-sentinel-with-log/requirement-sentinel-with-log.exception-processor';
import {CommonModule} from '@angular/common';
import {
  RequirementSentinelWithLogErrorRequirementHandler
} from '@ui-demo/core/modules/pages/requirement-sentinel-demo/requirement-sentinel-with-log/requirement-sentinel-with-log-error.requirement-handler';

@NgModule({
  imports: [
    RequirementSentinelModule.forRoot(),
    RequirementSentinelWithLogRoutingModule,
    ReactiveFormsModule,
    CommonModule
  ],
  declarations: [
    RequirementSentinelWithLogComponent
  ],
  providers: [
    {
      provide: REQUIREMENT_HANDLER,
      useClass: RequirementSentinelWithLogRequirementHandler,
      multi: true
    },
    {
      provide: REQUIREMENT_HANDLER,
      useClass: RequirementSentinelWithLogErrorRequirementHandler,
      multi: true
    },
    {
      provide: REQUIREMENT_SENTINEL_EXCEPTION_PROCESSOR,
      useClass: RequirementSentinelWithLogExceptionProcessor
    }
  ]
})
export class RequirementSentinelWithLogModule {

}
