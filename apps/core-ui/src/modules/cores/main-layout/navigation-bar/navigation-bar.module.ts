import {NgModule} from '@angular/core';
import {NavigationBarComponent} from '@ui-demo/core/modules/cores/main-layout/navigation-bar/navigation-bar.component';

@NgModule({
  exports: [
    NavigationBarComponent
  ],
  declarations: [
    NavigationBarComponent
  ]
})
export class NavigationBarModule {
}
