import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ValidationSummarizerWithFormArrayDemoPageComponent} from './validation-summarizer-with-form-array-demo-page.component';

const routes: Routes = [
  {
    path: '',
    component: ValidationSummarizerWithFormArrayDemoPageComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class ValidationSummarizerWithFormArrayDemoPageRoutingModule {

}
