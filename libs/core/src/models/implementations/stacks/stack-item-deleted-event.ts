import {IStackItemEvent} from '../../interfaces/stacks/stack-item-event.interface';
import {StackItemEvents} from '../../../constants';

export class StackItemDeletedEvent implements IStackItemEvent {

  //#region Properties

  public readonly kind: string;

  //#endregion


  //#region Constructor

  public constructor(
    public readonly stackCode: string,
    public readonly itemIds: string[]) {
    this.kind = StackItemEvents.ITEM__DELETED;
    this.stackCode = stackCode;
  }

  //#endregion

}
