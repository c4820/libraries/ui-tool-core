import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RequirementSentinelWithContextComponent} from './requirement-sentinel-with-context.component';

const routes: Routes = [
  {
    path: '',
    component: RequirementSentinelWithContextComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class RequirementSentinelWithContextRoutingModule {

}
