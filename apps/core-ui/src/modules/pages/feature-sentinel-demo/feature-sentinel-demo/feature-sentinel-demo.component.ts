import {ChangeDetectionStrategy, Component, Inject, Injectable, OnDestroy, OnInit} from '@angular/core';
import {UserFeatures} from '../user-features';
import {FeatureSentinelDemoFields} from './feature-sentinel-demo-fields';
import {FormGroup} from '@angular/forms';
import {Subscription} from 'rxjs';
import {AuthenticationService} from '../authentication.service';
import {FEATURE_SENTINEL_SERVICE, IFeatureSentinelService} from '@ui-tool/core';

@Component({
  selector: 'feature-sentinel-demo',
  templateUrl: 'feature-sentinel-demo.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class FeatureSentinelDemoComponent implements OnInit, OnDestroy {

  //#region Properties

  private readonly __fields = new FeatureSentinelDemoFields();

  private readonly __subscriptions = new Subscription();

  public readonly UserFeatures = UserFeatures;

  //#endregion

  //#region Accessors

  public get formGroup(): FormGroup {
    return this.__fields.formGroup;
  }

  //#endregion

  //#region Constructor

  public constructor(protected readonly _authenticationService: AuthenticationService,
                     @Inject(FEATURE_SENTINEL_SERVICE)
                     protected readonly _featureSentinelService: IFeatureSentinelService) {
  }

  //#endregion

  //#region Life cycle hook

  public ngOnInit(): void {
    const subscription = this.formGroup.valueChanges
      .subscribe(value => {
        const keys = Object.keys(value)
          .filter(key => value[key]);
        this._authenticationService.updateFeatures(keys);
        this._featureSentinelService.doValidation();
      });
    this.__subscriptions.add(subscription);
  }

  public ngOnDestroy(): void {
    this.__subscriptions.unsubscribe();
  }

  //#endregion
}
