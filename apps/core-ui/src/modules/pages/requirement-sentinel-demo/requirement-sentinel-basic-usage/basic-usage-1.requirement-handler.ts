import {IRequirementHandler} from '@ui-tool/core';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {
  RequirementSentinelBasicUsageQuery
} from '@ui-demo/core/modules/cores/stores/requirement-sentinel-basic-usage/requirement-sentinel-basic-usage.query';
import {
  RequirementSentinelBasicUsageNames
} from '@ui-demo/core/modules/pages/requirement-sentinel-demo/requirement-sentinel-basic-usage/requirement-sentinel-basic-usage-names';

@Injectable()
export class BasicUsage1RequirementHandler implements IRequirementHandler {

  //#region Properties

  public readonly name: string;

  //#endregion

  //#region Constructor

  public constructor(private readonly __query: RequirementSentinelBasicUsageQuery) {
    this.name = RequirementSentinelBasicUsageNames.BASIC_USAGE_1;
  }

  //#endregion

  //#region Methods

  public shouldRequirementMetAsync(): Observable<boolean> {
    return this.__query.usageOneEnabled$;
  }

  //#endregion
}
