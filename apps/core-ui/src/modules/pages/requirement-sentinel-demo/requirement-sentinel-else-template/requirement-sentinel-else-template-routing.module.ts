import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RequirementSentinelElseTemplateComponent} from './requirement-sentinel-else-template.component';

const routes: Routes = [
  {
    path: '',
    component: RequirementSentinelElseTemplateComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class RequirementSentinelElseTemplateRoutingModule {

}
