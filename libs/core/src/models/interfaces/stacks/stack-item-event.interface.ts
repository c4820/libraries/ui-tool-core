export interface IStackItemEvent {

  //#region Properties

  readonly kind: string;

  readonly stackCode: string;

  readonly itemIds: string[];

  //#endregion

}
