import {Pipe, PipeTransform} from '@angular/core';

@Pipe({
  name: 'toAny'
})
export class ToAnyPipe implements PipeTransform {

  //#region Methods

  public transform(value: any): any {
    return value as any;
  }

  //#endregion

}
