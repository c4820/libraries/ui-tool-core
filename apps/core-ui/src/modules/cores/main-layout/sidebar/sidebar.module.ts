import {NgModule} from '@angular/core';
import {SidebarComponent} from '@ui-demo/core/modules/cores/main-layout/sidebar/sidebar.component';
import {RouterModule} from '@angular/router';
import {SmartNavigatorModule} from '@ui-tool/core';

@NgModule({
  declarations: [SidebarComponent],
  imports: [
    RouterModule,
    SmartNavigatorModule
  ],
  exports: [
    SidebarComponent
  ]
})
export class SidebarModule {

}
