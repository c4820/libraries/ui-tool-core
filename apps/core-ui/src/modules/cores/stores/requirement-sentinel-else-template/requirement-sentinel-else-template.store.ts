import {Store, StoreConfig} from '@datorama/akita';
import {Injectable} from '@angular/core';
import {
  IRequirementSentinelElseTemplateState
} from '@ui-demo/core/modules/cores/stores/requirement-sentinel-else-template/requirement-sentinel-else-template.state';

@Injectable({
  providedIn: 'root'
})
@StoreConfig({name: 'requirement-sentinel__else-template'})
export class RequirementSentinelElseTemplateStore extends Store<IRequirementSentinelElseTemplateState> {

  //#region Constructor

  public constructor() {
    super({
      requirementSatisfied: false
    });
  }

  //#endregion

  //#region Methods

  public setRequirementSatisfaction(satisfied: boolean): void {
    this.update({
      requirementSatisfied: satisfied
    });
  }

  //#endregion
}
