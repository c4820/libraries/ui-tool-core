import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component, ComponentFactoryResolver, ComponentRef,
  Inject,
  Injector,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
  ViewContainerRef
} from '@angular/core';
import {v4 as uuid} from 'uuid';
import {Subject, Subscription} from 'rxjs';
import {
  DISPLAY_SPINNER_OPTIONS, SPINNER_HOST,
  SPINNER_METHOD_INVOKE_CALLBACK,
  SPINNER_REQUEST_ID,
  SPINNER_SERVICE
} from '../../constants/injectors/injectors';
import {ISpinnerService} from '../../services/interfaces/spinner-service.interface';
import {DisplaySpinnerCommand} from '../../models/implementations/spinners/display-spinner-command';
import {DeleteSpinnerCommand} from '../../models/implementations/spinners/delete-spinner-command';
import {BasicSpinnerComponent} from './basic-spinner/basic-spinner.component';
import {filter} from 'rxjs/operators';
import {SpinnerCommands} from '../../constants';
import {ISpinnerCommand, PurgeSpinnerCommand} from '../../models';

declare type SpinnerCommandContext = {
  command: DisplaySpinnerCommand,
  componentRef: ComponentRef<any> | null
};

@Component({
  selector: 'cms-spinner-container',
  template: '',
  styleUrls: ['spinner-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SpinnerContainerComponent implements OnInit, AfterViewInit, OnDestroy {

  //#region Properties

  // Id of loading spinner.
  private __id: string;

  // Whether spinner only displays the latest request only or not.
  private __preserveLatestRequest: boolean;

  // Class which is applied to host component.
  private __hostClass: string;

  // Subject which emits spinner visibility event.
  private readonly _visibilityEvent$: Subject<ISpinnerCommand | null>;

  // Mapping between display request id & displayed component.
  private readonly __contexts: SpinnerCommandContext[];

  // Subscription to handle local visibility request.
  private __localVisibilityRequestHandleSubscription: Subscription | undefined;

  // Subscription watch list.
  protected _visibilityChangedSubscription: Subscription | undefined;

  //#endregion

  //#region Accessors

  @Input()
  public set id(value: string) {
    this.__id = value;

    if (this._visibilityChangedSubscription && !this._visibilityChangedSubscription.closed) {
      this._visibilityChangedSubscription.unsubscribe();
      this._visibilityEvent$.next(undefined);
    }

    // Register spinner visibility changed event.
    this._visibilityChangedSubscription = this._spinnerService
      .hookSpinnerVisibilityEvent(value)
      .subscribe((event: DisplaySpinnerCommand | DeleteSpinnerCommand) => this._visibilityEvent$.next(event));
  }

  public get id(): string {
    return this.__id;
  }

  @Input()
  public set preserveLatestRequest(value: boolean) {
    this.__preserveLatestRequest = value;
  }

  public get preserveLatestRequest(): boolean {
    return this.__preserveLatestRequest;
  }

  //#endregion

  //#region Constructor

  public constructor(@Inject(SPINNER_SERVICE)
                     protected readonly _spinnerService: ISpinnerService,
                     protected readonly _viewContainerRef: ViewContainerRef,
                     protected readonly _changeDetectorRef: ChangeDetectorRef,
                     private readonly __injector: Injector) {
    this.id = uuid();

    this.__id = uuid();
    this.__hostClass = '';
    this.__preserveLatestRequest = true;

    this.__contexts = [];
    this._visibilityEvent$ = new Subject<ISpinnerCommand | null>();
  }

  //#endregion

  //#region Life cycle hooks

  public ngOnInit(): void {

    // Subscription registration.
    this.__localVisibilityRequestHandleSubscription = this._visibilityEvent$
      .pipe(
        filter(command => command?.containerId === this.__id),
        filter(command => command !== null && command !== undefined),
      )
      .subscribe(command => {
        this._handleVisibilityChangedEvent(command);
        this._changeDetectorRef.markForCheck();
      });
  }

  public ngAfterViewInit(): void {
    // Update component id to trigger spinner event.
    this.id = this.__id || uuid();
  }

  //#endregion

  //#region Methods

  // Called when component is destroyed.
  public ngOnDestroy(): void {

    this._visibilityChangedSubscription?.unsubscribe();
    this._visibilityEvent$?.unsubscribe();
    this.__localVisibilityRequestHandleSubscription?.unsubscribe();
  }

  // Handle visibility changed event.
  protected _handleVisibilityChangedEvent(command: ISpinnerCommand | null): void {

    // Invalid command & view container ref.
    if (!command || !this._viewContainerRef) {
      return;
    }

    if (command.kind === SpinnerCommands.display) {
      const actualCommand = command as DisplaySpinnerCommand;

      let purge = false;
      if (actualCommand.options) {
        purge = actualCommand.options.purge || false;
      }

      if (purge) {
        const purgeRequest = new PurgeSpinnerCommand(actualCommand.containerId);
        this._handleVisibilityChangedEvent(purgeRequest);
      } else if (this.__preserveLatestRequest) {
        // Get the latest context.
        const context = this._getLatestContext();
        context?.context?.componentRef?.destroy();
      }

      this._displaySpinner(actualCommand, -1);
      return;
    }

    if (command.kind === SpinnerCommands.close) {
      const actualCommand = command as DeleteSpinnerCommand;
      if (actualCommand.id) {
        this._dismissByRequestId(actualCommand.id);
      }

      // There is at least one display request. Display that one.
      const {context, index: latestIndex} = this._getLatestContext();
      if (context && latestIndex > -1) {
        this._displaySpinner(context.command, latestIndex);
      }

      return;
    }

    if (command.kind === SpinnerCommands.purge) {
      while (true) {
        if (!this.__contexts.length) {
          break;
        }

        this._dismissByRequestId(this.__contexts[0].command?.id, true);
      }

      return;
    }
  }

  protected _displaySpinner(displaySpinnerRequest: DisplaySpinnerCommand, index: number): void {

    if (!displaySpinnerRequest || !this._viewContainerRef) {
      return;
    }

    const commandContext: SpinnerCommandContext = {
      command: displaySpinnerRequest,
      componentRef: null
    };

    if (!(index < 0 || index >= this.__contexts.length)) {
      this.__contexts[index].componentRef?.destroy();
    }

    const childInjector = Injector.create({
      providers: [
        {
          provide: SPINNER_METHOD_INVOKE_CALLBACK,
          useValue: displaySpinnerRequest.options?.invokedMethod
        },
        {
          provide: SPINNER_REQUEST_ID,
          useValue: displaySpinnerRequest.id
        },
        {
          provide: DISPLAY_SPINNER_OPTIONS,
          useValue: displaySpinnerRequest.options
        },
        {
          provide: SPINNER_HOST,
          useValue: displaySpinnerRequest.containerId
        }
      ],
      parent: this.__injector
    });

    if (!displaySpinnerRequest.options || !displaySpinnerRequest.options.instanceType) {
      const componentRef = this._viewContainerRef.createComponent(BasicSpinnerComponent, {
        injector: childInjector
      });
      componentRef.changeDetectorRef.markForCheck();
      commandContext.componentRef = componentRef;
    } else {
      const componentRef = this._viewContainerRef.createComponent(displaySpinnerRequest.options.instanceType, {
        injector: childInjector
      });
      componentRef.changeDetectorRef.markForCheck();
      commandContext.componentRef = componentRef;
    }

    if (index < 0 || index >= this.__contexts.length) {
      this.__contexts.push(commandContext);
    } else {
      this.__contexts[index] = commandContext;
    }
  }

  protected _dismissByRequestId(requestId: string, force?: boolean): void {
    const index = this.__contexts.findIndex(x => x.command?.id === requestId);
    if (index < 0) {
      return;
    }

    const context = this.__contexts[index];
    if (!context) {
      return;
    }

    const command = context.command;
    if (!command || command.kind !== SpinnerCommands.display) {
      return;
    }

    // Spinner cannot be closed.
    const closingHandler = command?.options?.closing;
    if (!force && closingHandler && !closingHandler()) {
      return;
    }

    context.componentRef?.destroy();
    context.componentRef = null;
    command?.options?.closed?.(force || false);
    this.__contexts.splice(index, 1);
  }

  protected _getLatestContext(): { context: SpinnerCommandContext | null, index: number } {
    if (this.__contexts.length < 1) {
      return {context: null, index: -1};
    }

    const lastIndex = this.__contexts.length - 1;
    return {context: this.__contexts[lastIndex], index: lastIndex};
  }

  //#endregion

}
