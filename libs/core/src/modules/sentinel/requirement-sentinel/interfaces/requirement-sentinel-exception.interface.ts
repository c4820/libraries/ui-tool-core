import {RequirementSentinelExceptionTypes} from '../constants/requirement-sentinel-exception-types';

export interface IRequirementSentinelException<T> {

  //#region Properties

  readonly type: RequirementSentinelExceptionTypes;

  readonly code: string;

  readonly requirementName: string;

  metadata?: T;

  //#endregion

}
