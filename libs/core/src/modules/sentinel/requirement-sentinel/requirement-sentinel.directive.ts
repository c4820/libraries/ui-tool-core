import {Directive, Inject, Input, OnDestroy, OnInit, TemplateRef, ViewContainerRef} from '@angular/core';
import {REQUIREMENT_SENTINEL_SERVICE} from '../../../constants/injectors/injectors';
import {of, Subject, Subscription} from 'rxjs';
import {catchError, distinctUntilChanged, switchMap} from 'rxjs/operators';
import {IMeetRequirementService} from './requirement-sentinel-service.interface';

@Directive({
  selector: '[meetRequirement]'
})
export class RequirementSentinelDirective implements OnInit, OnDestroy {

  //#region Properties

  // Name of requirement which must be satisfied.
  private __requirement: string | undefined;

  private __elseTemplateRef?: TemplateRef<any>;

  private __context?: object;

  // Raise event about displaying feature content.
  private __displayContent$: Subject<void>;

  // Subscription watch list.
  private readonly __subscription: Subscription;

  //#endregion

  //#region Accessors

  @Input('meetRequirement')
  public set requirement(value: string) {
    this.__requirement = value;
    this.__displayContent$.next();
  }

  @Input('meetRequirementElse')
  public set elseTemplate(value: TemplateRef<any>) {
    this.__elseTemplateRef = value;
    this.__displayContent$.next();
  }

  @Input('meetRequirementContext')
  public set context(value: object | undefined) {
    this.__context = value;
    this.__displayContent$.next();
  }

  //#endregion

  //#region Constructor

  public constructor(
    @Inject(REQUIREMENT_SENTINEL_SERVICE)
    protected readonly _meetRequirementService: IMeetRequirementService,
    protected readonly _viewContainerRef: ViewContainerRef,
    protected readonly _templateRef: TemplateRef<any>) {
    this.__requirement = undefined;
    this.__displayContent$ = new Subject<void>();
    this.__subscription = new Subscription();
  }

  //#endregion

  //#region Life cycles

  public ngOnInit(): void {

    // Display feature subscription.
    const displayContentSubscription = this.__displayContent$
      .pipe(
        switchMap(() => {
          return this._meetRequirementService
            .shouldRequirementMetAsync(this.__requirement || '', this.__context)
            .pipe(
              catchError(_ => of(false))
            );
        }),
        distinctUntilChanged()
      )
      .subscribe(ableToAccessFeature => {
        this._viewContainerRef.clear();
        if (!ableToAccessFeature) {
          if (this.__elseTemplateRef) {
            const elseView = this._viewContainerRef.createEmbeddedView(this.__elseTemplateRef);
            elseView.markForCheck();
          }
          return;
        }

        const embeddedView = this._viewContainerRef.createEmbeddedView(this._templateRef);
        embeddedView.markForCheck();
      });
    this.__subscription.add(displayContentSubscription);

    // Hook validation event from service.
    const hookValidationEventSubscription = this._meetRequirementService
      .hookValidationEventAsync()
      .subscribe(() => {
        this.__displayContent$.next();
      });
    this.__subscription.add(hookValidationEventSubscription);


    // Do the first requirement check.
    this.__displayContent$.next();
  }

  public ngOnDestroy(): void {
    this.__subscription?.unsubscribe();
  }

  //#endregion
}
