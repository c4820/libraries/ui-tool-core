import {NgModule} from '@angular/core';
import {ToTrustedStylePipe} from './to-trusted-style.pipe';

@NgModule({
  declarations: [
    ToTrustedStylePipe
  ],
  exports: [
    ToTrustedStylePipe
  ]
})
export class ToTrustedStylePipeModule {

}
