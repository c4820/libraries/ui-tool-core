export enum RequirementSentinelWithChildModulesNames {
  PARENT = 'PARENT',
  CHILD = 'CHILD'
}
