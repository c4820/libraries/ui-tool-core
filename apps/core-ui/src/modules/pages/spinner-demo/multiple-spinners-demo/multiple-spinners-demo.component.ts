import {ChangeDetectionStrategy, Component, Inject} from '@angular/core';
import {SPINNER_SERVICE, ISpinnerService} from '@ui-tool/core';
import {ToastrService} from 'ngx-toastr';
import {
  InlineDismissibleSpinnerComponent
} from '@ui-demo/core/modules/pages/spinner-demo/custom-spinners/inline-dismissible-spinner/inline-dismissible-spinner.component';

@Component({
  selector: 'spinner-with-method-invoke-demo-event-demo',
  templateUrl: 'multiple-spinners-demo.component.html',
  styleUrls: ['./multiple-spinners-demo.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MultipleSpinnersDemoComponent {

  //#region Properties

  private __index = 0;

  private readonly __spinnerRequestIds: string[];

  public readonly demoSpinnerId = 'DEMO_SPINNER_ID';

  public purgeOldRequests: boolean;

  //#endregion

  //#region Constructor

  public constructor(@Inject(SPINNER_SERVICE)
                     protected readonly _spinnerService: ISpinnerService,
                     protected readonly _toastService: ToastrService) {
    this.purgeOldRequests = false;
    this.__spinnerRequestIds = [];
  }

  //#endregion

  //#region Methods

  public displaySpinner(): void {
    const spinnerRequestId = this._spinnerService.displaySpinner(this.demoSpinnerId, {
      instanceType: ((this.__index++) % 2 === 0) ? undefined : InlineDismissibleSpinnerComponent,
      closed: (force: boolean) => {

        const index = this.__spinnerRequestIds.findIndex(x => x === spinnerRequestId);
        if (index < 0) {
          return;
        }

        this.__spinnerRequestIds.splice(index, 1);
        if (!force) {
          this._toastService.success('Spinner closed !');
        } else {
          this._toastService.warning('Spinner was forced to be closed because of purging.');
        }
      },
      purge: this.purgeOldRequests
    });
    this.__spinnerRequestIds.push(spinnerRequestId);
  }

  public deleteSpinner(): void {

    const length = this.__spinnerRequestIds.length;
    if (length < 1) {
      return;
    }

    const spinnerRequestId = this.__spinnerRequestIds[length - 1];
    this._spinnerService.deleteSpinner(this.demoSpinnerId, spinnerRequestId);
  }

  //#endregion
}
