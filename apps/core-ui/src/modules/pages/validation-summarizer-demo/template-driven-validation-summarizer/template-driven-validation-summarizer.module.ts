import {NgModule} from '@angular/core';
import {TemplateDrivenValidationSummarizerComponent} from './template-driven-validation-summarizer.component';
import {RouterModule} from '@angular/router';
import {ValidationSummarizerModule, CommonValidatorModule, HasAnyValidatorsModule} from '@ui-tool/core';
import {FormsModule} from '@angular/forms';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: TemplateDrivenValidationSummarizerComponent
      }
    ]),
    ValidationSummarizerModule.forChild({}),
    FormsModule,
    CommonValidatorModule.forChild({
      defaultValidationClasses: ['text-danger'],
      defaultControlValidationClasses: ['border', 'border-danger']
    }),
    HasAnyValidatorsModule,
    HasAnyValidatorsModule,
    HasAnyValidatorsModule
  ],
  declarations: [
    TemplateDrivenValidationSummarizerComponent
  ]
})
export class TemplateDrivenValidationSummarizerModule {

}
