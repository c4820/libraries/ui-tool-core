import {IMeetRequirementService} from './requirement-sentinel-service.interface';
import {Observable, of, ReplaySubject, Subject} from 'rxjs';
import {InjectFlags, Injector} from '@angular/core';
import {IRequirementHandler} from './requirement-handler.interface';
import {REQUIREMENT_HANDLER, REQUIREMENT_SENTINEL_EXCEPTION_PROCESSOR} from '../../../constants/injectors/injectors';
import {IRequirementSentinelException} from './interfaces/requirement-sentinel-exception.interface';
import {IRequirementSentinelExceptionProcessor} from './requirement-sentinel-exception-processor.interface';
import {catchError, map} from 'rxjs/operators';
import {InternalRequirementSentinelException} from './interfaces/internal-requirement-sentinel.exception';
import {RequirementSentinelExceptionCodes} from './constants/requirement-sentinel-exception-codes';
import {ExternalRequirementSentinelException} from './interfaces/external-requirement-sentinel.exception';

export class RequirementSentinelService implements IMeetRequirementService {

  //#region Properties

  protected readonly _requirementHandlers: IRequirementHandler[];

  protected readonly _exceptionProcessor?: IRequirementSentinelExceptionProcessor;

  protected readonly _doValidationEvent: Subject<void>;

  //#endregion

  //#region Constructor

  public constructor(injector: Injector) {
    this._requirementHandlers = injector
      .get(REQUIREMENT_HANDLER, null, InjectFlags.Optional) as unknown as IRequirementHandler[];
    this._exceptionProcessor = injector.get(REQUIREMENT_SENTINEL_EXCEPTION_PROCESSOR, null
      , InjectFlags.Optional) as unknown as IRequirementSentinelExceptionProcessor;
    this._doValidationEvent = new ReplaySubject(1);
  }

  //#endregion

  //#region Methods

  public shouldRequirementMetAsync<TContext>(name: string, context?: object): Observable<boolean> {

    // No requirement is found.
    if (!this._requirementHandlers || !this._requirementHandlers.length) {
      return this.__processExceptionAsync(
        new InternalRequirementSentinelException(name, RequirementSentinelExceptionCodes.NO_REQUIREMENT_HANDLER_AT_ALL));
    }
    // Find the requirement which has the exact name  with the passed one.
    const requirementHandler = this._requirementHandlers.find(item => item.name === name);
    if (!requirementHandler) {
      return this.__processExceptionAsync(
        new InternalRequirementSentinelException(name, RequirementSentinelExceptionCodes.REQUIREMENT_NOT_FOUND_FOR_SPECIFIC_NAME, {
          name
        }));
    }

    return requirementHandler.shouldRequirementMetAsync(context)
      .pipe(
        catchError(exception => {
          return this.__processExceptionAsync(
            new ExternalRequirementSentinelException(name, RequirementSentinelExceptionCodes.FAILED_WHILE_HANDLING_EXTERNAL_BUSINESS, {
              exception
            }));
        })
      );
  }

  public doValidation(): void {
    this._doValidationEvent.next();
  }

  public hookValidationEventAsync(): Observable<void> {
    return this._doValidationEvent.asObservable();
  }

  //#endregion

  //#region Internal methods

  private __processExceptionAsync<T>(exceptionInstance: IRequirementSentinelException<T>): Observable<boolean> {
    if (!this._exceptionProcessor) {
      return of(false);
    }

    return this._exceptionProcessor.processExceptionAsync(exceptionInstance)
      .pipe(
        map(() => false)
      );
  }

  //#endregion
}
