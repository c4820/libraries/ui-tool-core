import {ChangeDetectionStrategy, Component, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {
  RequirementSentinelElseTemplateNames
} from '@ui-demo/core/modules/pages/requirement-sentinel-demo/requirement-sentinel-else-template/requirement-sentinel-else-template-names';
import {
  RequirementSentinelElseTemplateStore
} from '@ui-demo/core/modules/cores/stores/requirement-sentinel-else-template/requirement-sentinel-else-template.store';

@Component({
  selector: 'requirement-sentinel-else-template',
  templateUrl: 'requirement-sentinel-else-template.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequirementSentinelElseTemplateComponent implements OnInit, OnDestroy {

  //#region Properties

  private readonly __subscriptions = new Subscription();

  public readonly RequirementNames = RequirementSentinelElseTemplateNames;

  //#endregion

  //#region Constructor

  public constructor(private readonly __store: RequirementSentinelElseTemplateStore) {
  }

  //#endregion

  //#region Life cycle hook

  public ngOnInit(): void {

  }

  public ngOnDestroy(): void {
    this.__subscriptions.unsubscribe();
  }

  //#endregion

  //#region Methods

  public dissatisfyRequirement(): void {
    this.__store.setRequirementSatisfaction(false);
  }

  public satisfyRequirement(): void {
    this.__store.setRequirementSatisfaction(true);

  }

  //#endregion
}
