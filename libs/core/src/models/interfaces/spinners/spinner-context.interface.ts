export interface ISpinnerContext {

  //#region Properties

  // Mapping between name & command.
  nameToCommand: Record<string, () => void>;

  //#endregion

}
