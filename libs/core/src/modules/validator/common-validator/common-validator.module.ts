import {ModuleWithProviders, NgModule} from '@angular/core';
import {WithInvalidValidatorClassNotFormControlDirective} from './validation-summarizer-directive/with-invalid-validator-class-not-form-control.directive';
import {ValidatorControlWatchDirective} from './validation-summarizer-directive/validator-control-watch.directive';
import {IValidationSummarizerModuleOptions} from '../../../models/interfaces/validation-summarizers/validation-summarizer-module-options.interface';
import {
  buildCommonValidatorOptions,
  buildCommonValidatorOptionsProvider,
  buildCommonValidatorService
} from '../../../factories/common-validator.factory';
import {NULL_COMMON_VALIDATOR_SERVICE} from '../../../constants/injectors/internal-injectors';
import {
  WithInvalidValidatorClassInFormControlDirective
} from './validation-summarizer-directive/with-invalid-validator-class-in-form-control.directive';
import {ICommonValidatorModuleOptions} from '../../../models';

@NgModule({
  declarations: [
    WithInvalidValidatorClassInFormControlDirective,
    WithInvalidValidatorClassNotFormControlDirective,
    ValidatorControlWatchDirective
  ],
  exports: [
    WithInvalidValidatorClassInFormControlDirective,
    WithInvalidValidatorClassNotFormControlDirective,
    ValidatorControlWatchDirective
  ]
})
export class CommonValidatorModule {

  //#region Methods

  public static forRoot(
    options?: Partial<ICommonValidatorModuleOptions>)
    : ModuleWithProviders<CommonValidatorModule> {
    return {
      ngModule: CommonValidatorModule,
      providers: [
        // Options.
        buildCommonValidatorOptions(options),

        // Options provider.
        buildCommonValidatorOptionsProvider(),

        // Build validator provider.
        options?.validatorProvider || buildCommonValidatorService()
      ]
    };
  }

  public static forChild(
    options?: ICommonValidatorModuleOptions)
    : ModuleWithProviders<CommonValidatorModule> {
    return {
      ngModule: CommonValidatorModule,
      providers: [
        // Options.
        buildCommonValidatorOptions(options),

        // Options provider.
        buildCommonValidatorOptionsProvider(),

        // Build validator provider.
        options?.validatorProvider ? options.validatorProvider : {
          provide: NULL_COMMON_VALIDATOR_SERVICE,
          useValue: null
        }
      ]
    };
  }

  //#endregion
}
