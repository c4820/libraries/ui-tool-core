import {RouterModule, Routes} from '@angular/router';
import {NgModule} from '@angular/core';

const routes: Routes = [
  {
    path: 'with-method-invoke',
    loadChildren: () => import('./spinner-with-method-invoke-demo/spinner-with-method-invoke-demo.module')
      .then(m => m.SpinnerWithMethodInvokeDemoModule)
  },
  {
    path: 'multiple-spinners',
    loadChildren: () => import('./multiple-spinners-demo/multiple-spinners-demo.module')
      .then(m => m.MultipleSpinnersDemoModule)
  },
  {
    path: '',
    loadChildren: () => import('./spinner-with-event-demo/spinner-with-event-demo.module')
      .then(m => m.SpinnerWithEventDemoModule)
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class SpinnerDemoRoutingModule {

}
