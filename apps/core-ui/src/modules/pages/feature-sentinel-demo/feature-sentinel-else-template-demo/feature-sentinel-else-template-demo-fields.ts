import {FormControl, FormGroup} from '@angular/forms';
import {UserFeatures} from '../user-features';

export class FeatureSentinelElseTemplateDemoFields {

  //#region Properties

  private readonly __formGroup: FormGroup;

  //#endregion

  //#region Accessors

  public get formGroup(): FormGroup {
    return this.__formGroup;
  }

  //#endregion

  //#region Constructor

  public constructor() {
    this.__formGroup = new FormGroup({
      [UserFeatures.addUser]: new FormControl(false),
      [UserFeatures.editAnyUser]: new FormControl(false),
      [UserFeatures.deleteAnyUser]: new FormControl(false)
    });
  }

  //#endregion

}
