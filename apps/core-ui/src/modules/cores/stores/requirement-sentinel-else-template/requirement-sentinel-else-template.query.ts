import {Query} from '@datorama/akita';
import {Injectable} from '@angular/core';
import {
  RequirementSentinelBasicUsageStore
} from '@ui-demo/core/modules/cores/stores/requirement-sentinel-basic-usage/requirement-sentinel-basic-usage.store';
import {
  IRequirementSentinelElseTemplateState
} from '@ui-demo/core/modules/cores/stores/requirement-sentinel-else-template/requirement-sentinel-else-template.state';
import {
  RequirementSentinelElseTemplateStore
} from '@ui-demo/core/modules/cores/stores/requirement-sentinel-else-template/requirement-sentinel-else-template.store';

@Injectable({
  providedIn: 'root'
})
export class RequirementSentinelElseTemplateQuery extends Query<IRequirementSentinelElseTemplateState> {

  //#region Properties

  public readonly requirementSatisfied$ = this.select(m => m.requirementSatisfied);

  //#endregion

  //#region Constructor

  public constructor(store: RequirementSentinelElseTemplateStore) {
    super(store);
  }

  //#endregion
}
