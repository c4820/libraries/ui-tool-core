import { ISmartNavigatorService } from '../interfaces/smart-navigator-service.interface';
import { InjectFlags, Injector } from '@angular/core';
import { from, Observable } from 'rxjs';
import { NavigationExtras, Router, UrlTree } from '@angular/router';
import { merge as lodashMerge } from 'lodash-es';
import { SMART_NAVIGATOR_ROUTES, SMART_NAVIGATOR_SCREEN_CODE_RESOLVER } from '../../constants/injectors/injectors';
import { NavigateToScreenRequest } from '../../models/implementations/smart-navigators/navigate-to-screen-request';
import { IScreenCodeResolver } from '../interfaces/screen-code-resolver.interface';
import { SmartNavigatorExceptions } from '../../constants/smart-navigator-exceptions';

export class SmartNavigatorService implements ISmartNavigatorService {

  //#region Properties

  private readonly __codeToUrlMappings: { [key: string]: string; };

  protected _router: Router;

  protected _screenCodeResolvers: IScreenCodeResolver[];


  //#endregion

  //#region Constructor

  public constructor(protected injector: Injector) {

    const codeToUrlMappings = this.injector.get(SMART_NAVIGATOR_ROUTES);
    this._router = this.injector.get(Router);
    this._screenCodeResolvers = this.injector.get(SMART_NAVIGATOR_SCREEN_CODE_RESOLVER,
      null, InjectFlags.Optional) as any as IScreenCodeResolver[];
    this.__codeToUrlMappings = {};

    if (codeToUrlMappings) {
      this.__codeToUrlMappings = lodashMerge({}, codeToUrlMappings);
    }
  }

  //#endregion

  //#endregion

  //#region Methods

  // Navigate to a screen by using screen code.
  public navigateToScreenAsync(request: NavigateToScreenRequest<any>): Observable<boolean> {

    if (!request) {
      throw new Error(SmartNavigatorExceptions.invalidNavigationRequest);
    }

    // Get raw url from screen code.
    const rawUrl = this.loadRawUrl(request.code);

    const compiled = this.__template(rawUrl);
    const fullUrl = compiled(request.routeParams);

    return from(this._router.navigate([fullUrl], request.extras));
  }

  // Get raw url.
  public loadRawUrl(code: string): string {
    if (!code || !code.length) {
      throw new Error(SmartNavigatorExceptions.invalidScreenCode);
    }

    let url: string | null = this.__codeToUrlMappings[code];
    if (url) {
      return url;
    }

    const screenCodeResolvers = this._screenCodeResolvers;
    if (!screenCodeResolvers || !screenCodeResolvers.length) {
      throw new Error(SmartNavigatorExceptions.invalidScreenCode);
    }

    for (const screenCodeResolver of screenCodeResolvers) {
      url = screenCodeResolver.loadUrl(code);
      if (url && url.length) {
        return url;
      }
    }

    throw new Error(SmartNavigatorExceptions.invalidScreenCode);
  }

  // Build url tree.
  public buildUrlTree(screenCode: string, routeParams?: { [key: string]: any; },
                      extras?: NavigationExtras): UrlTree {
    const rawUrl = this.loadRawUrl(screenCode) || '';
    const compiled = this.__template(rawUrl);
    const fullUrl = compiled(routeParams);

    const urlTree = this._router.createUrlTree([fullUrl], extras);
    return urlTree;
  }

  public buildUrlTreeFromNavigationRequest<T>(navigationRequest: NavigateToScreenRequest<T>): UrlTree {
    return this.buildUrlTree(navigationRequest.code, navigationRequest.routeParams, navigationRequest.extras);
  }

  private __template(value: string): (params?: Record<string, any>) => string {
    return (params?: Record<string, any>): string => {
      let rawValue = `${value ?? ''}`;
      const rawParams = params || {};
      Object.keys(rawParams).forEach(key => {
        rawValue = rawValue.split('{{' + key + '}}').join(rawParams[key]);
      });
      return rawValue;
    };
  }

  //#endregion
}
