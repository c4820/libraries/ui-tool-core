import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {MainLayoutComponent} from './cores/main-layout/main-layout.component';
import {AppGuard} from '@ui-demo/core/guards/app.guard';

const routes: Routes = [
  {
    path: '',
    component: MainLayoutComponent,
    canActivate: [AppGuard],
    children: [
      {
        path: 'validation-summarizer',
        loadChildren: () => import('./pages/validation-summarizer-demo/validation-summarizer-demo.module')
          .then(m => m.ValidationSummarizerDemoModule)
      },
      {
        path: 'feature-sentinel',
        loadChildren: () => import('./pages/feature-sentinel-demo/feature-sentinel-demo.module')
          .then(m => m.FeatureSentinelDemoModule)
      },
      {
        path: 'role-sentinel',
        loadChildren: () => import('./pages/role-sentinel-demo/role-sentinel-demo.module')
          .then(m => m.RoleSentinelDemoModule)
      },
      {
        path: 'requirement-sentinel',
        loadChildren: () => import('./pages/requirement-sentinel-demo/requirement-sentinel-demo.module')
          .then(m => m.RequirementSentinelDemoModule)
      },
      {
        path: 'spinner',
        loadChildren: () => import('./pages/spinner-demo/spinner-demo.module')
          .then(m => m.SpinnerDemoModule)
      },
      {
        path: 'stack',
        loadChildren: () => import('./pages/stack-demo/stack-demo.module')
          .then(m => m.StackDemoModule)
      },
      {
        path: 'multiple-validation-summarizer',
        loadChildren: () => import('./pages/multiple-validation-summarizer-demo/multiple-validation-summarizer-demo.module')
          .then(m => m.MultipleValidationSummarizerDemoModule)
      },
      {
        path: 'window',
        loadChildren: () => import('./pages/window/window.module')
          .then(m => m.WindowModule)
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
