import { DefaultScreenCodeResolver } from '@ui-tool/core';
import { Injectable } from '@angular/core';
import {ScreenCodes} from '@ui-demo/core/constants/screen-codes';

@Injectable()
export class ScreenCodeResolver extends DefaultScreenCodeResolver {
  //#region Properties

  //#endregion

  //#region Constructor

  public constructor() {
    const codeToUrl: Record<string, string> = {};

    codeToUrl[ScreenCodes.HOME] = '/';
    codeToUrl[ScreenCodes.VALIDATION_SUMMARIZER__BASIC] = '/validation-summarizer';
    codeToUrl[ScreenCodes.VALIDATION_SUMMARIZER__TEMPLATE_DRIVEN] = '/validation-summarizer/template-driven';
    codeToUrl[ScreenCodes.VALIDATION_SUMMARIZER__VALIDATION_CLASS] = '/validation-summarizer/class-demo';
    codeToUrl[ScreenCodes.VALIDATION_SUMMARIZER__FORM_ARRAY] = '/validation-summarizer/form-array-demo';

    codeToUrl[ScreenCodes.MULTIPLE_VALIDATION_SUMMARIZER__BASIC] = '/multiple-validation-summarizer';

    codeToUrl[ScreenCodes.FEATURE_SENTINEL__BASIC_USAGE] = '/feature-sentinel';
    codeToUrl[ScreenCodes.FEATURE_SENTINEL__WITH_ELSE_TEMPLATE] = '/feature-sentinel/with-else-template';

    codeToUrl[ScreenCodes.ROLE_SENTINEL__BASIC_USAGE] = '/role-sentinel';
    codeToUrl[ScreenCodes.ROLE_SENTINEL__WITH_ELSE_TEMPLATE] = '/role-sentinel/with-else-template';

    codeToUrl[ScreenCodes.SPINNER__BASIC_USAGE] = '/spinner';
    codeToUrl[ScreenCodes.SPINNER__MULTIPLE_SPINNERS] = '/spinner/multiple-spinners';
    codeToUrl[ScreenCodes.SPINNER__METHOD_INVOKE] = '/spinner/with-method-invoke';

    codeToUrl[ScreenCodes.REQUIREMENT_SENTINEL__BASIC_USAGE] = '/requirement-sentinel';
    codeToUrl[ScreenCodes.REQUIREMENT_SENTINEL__WITH_ELSE_TEMPLATE] = '/requirement-sentinel/with-else-template';
    codeToUrl[ScreenCodes.REQUIREMENT_SENTINEL__WITH_LOG] = '/requirement-sentinel/with-log';
    codeToUrl[ScreenCodes.REQUIREMENT_SENTINEL__WITH_CONTEXT] = '/requirement-sentinel/with-context';
    codeToUrl[ScreenCodes.REQUIREMENT_SENTINEL__WITH_CHILD_MODULES] = '/requirement-sentinel/with-child-modules';

    codeToUrl[ScreenCodes.STACK__BASIC_USAGE] = '/stack';

    super(codeToUrl);
  }
  //#endregion
}
