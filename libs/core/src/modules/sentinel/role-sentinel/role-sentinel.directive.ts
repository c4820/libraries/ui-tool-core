import {Directive, Inject, Input, OnDestroy, OnInit, TemplateRef, ViewContainerRef} from '@angular/core';
import {ROLE_SENTINEL_SERVICE} from '../../../constants/injectors/injectors';
import {of, ReplaySubject, Subject, Subscription} from 'rxjs';
import {catchError, debounceTime, distinctUntilChanged, switchMap} from 'rxjs/operators';
import {IRoleSentinelService} from './role-sentinel-service.interface';

@Directive({
  selector: '[hasRoles]'
})
export class RoleSentinelDirective implements OnInit, OnDestroy {

  //#region Properties

  // Name of feature.
  private __names: string[];

  // Raise event about displaying feature content.
  private __displayRoleContent$: Subject<void>;

  private __elseTemplateRef?: TemplateRef<any>;

  // Subscription watch list.
  protected readonly _subscription: Subscription;

  //#endregion

  //#region Accessors

  @Input('hasRoles')
  public set name(value: string | string[]) {

    if (value instanceof Array) {
      this.__names = value as string[];
    } else {
      this.__names = [value];
    }

    this.__displayRoleContent$.next();
  }

  @Input('hasRolesElse')
  public set elseTemplate(value: TemplateRef<any>) {
    this.__elseTemplateRef = value;
    this.__displayRoleContent$.next();
  }

  //#endregion

  //#region Constructor

  public constructor(@Inject(ROLE_SENTINEL_SERVICE)
                     protected readonly _requireRolePermissionService: IRoleSentinelService,
                     protected readonly _viewContainerRef: ViewContainerRef,
                     protected readonly _templateRef: TemplateRef<any>) {

    this.__names = [];
    this.__displayRoleContent$ = new ReplaySubject<void>(1);
    this._subscription = new Subscription();
  }

//#endregion

  //#region Methods

  public ngOnInit(): void {

    const displayFeatureContentSubscription = this.__displayRoleContent$
      .pipe(
        switchMap(() => {
          return this._requireRolePermissionService
            .hasAnyRoleAsync(this.__names)
            .pipe(
              catchError(_ => of(false))
            );
        }),
        distinctUntilChanged()
      )
      .subscribe(ableToAccessFeature => {
        this._viewContainerRef.clear();

        if (!ableToAccessFeature) {
          if (this.__elseTemplateRef) {
            const elseView = this._viewContainerRef.createEmbeddedView(this.__elseTemplateRef);
            elseView.markForCheck();
          }
          return;
        }

        const embeddedView = this._viewContainerRef.createEmbeddedView(this._templateRef);
        embeddedView.markForCheck();
      });

    // Trigger validation.
    this.__displayRoleContent$.next();

    const hookRoleValidationSubscription = this._requireRolePermissionService
      .hookValidationEventAsync()
      .subscribe(() => {
        this.__displayRoleContent$.next();
      });


    this._subscription.add(hookRoleValidationSubscription);
    this._subscription.add(displayFeatureContentSubscription);
  }

  public ngOnDestroy(): void {
    this._subscription?.unsubscribe();
    this.__displayRoleContent$.complete();
  }

  //#endregion
}
