import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {combineLatest, Subscription} from 'rxjs';
import {
  RequirementSentinelWithLogStore
} from '@ui-demo/core/modules/cores/stores/requirement-sentinel-with-log/requirement-sentinel-with-log.store';
import {
  RequirementSentinelWithLogNames
} from '@ui-demo/core/modules/pages/requirement-sentinel-demo/requirement-sentinel-with-log/requirement-sentinel-with-log-names';
import {IRequirementSentinelException, RequirementSentinelExceptionTypes} from '@ui-tool/core';
import {
  RequirementSentinelWithLogQuery
} from '@ui-demo/core/modules/cores/stores/requirement-sentinel-with-log/requirement-sentinel-with-log.query';

@Component({
  selector: 'requirement-sentinel-with-log',
  templateUrl: 'requirement-sentinel-with-log.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequirementSentinelWithLogComponent implements OnInit, OnDestroy {

  //#region Properties

  private __internalExceptions: IRequirementSentinelException<any>[];
  private __externalExceptions: IRequirementSentinelException<any>[];

  private readonly __subscriptions = new Subscription();

  public readonly RequirementNames = RequirementSentinelWithLogNames;

  public readonly RequirementSentinelExceptionTypes = RequirementSentinelExceptionTypes;

  //#endregion

  //#region Accessors

  public get internalExceptions(): IRequirementSentinelException<any>[] {
    return this.__internalExceptions;
  }

  public get externalExceptions(): IRequirementSentinelException<any>[] {
    return this.__externalExceptions;
  }

  //#endregion

  //#region Constructor

  public constructor(private readonly __store: RequirementSentinelWithLogStore,
                     private readonly __query: RequirementSentinelWithLogQuery,
                     private readonly __changeDetectorRef: ChangeDetectorRef) {
    this.__internalExceptions = [];
    this.__externalExceptions = [];
  }

  //#endregion

  //#region Life cycle hook

  public ngOnInit(): void {


    const getExceptionMessagesSubscription = combineLatest([this.__query.internalExceptions$, this.__query.externalExceptions$])
      .subscribe(([internalExceptions, externalExceptions]) => {
        this.__internalExceptions = internalExceptions;
        this.__externalExceptions = externalExceptions;
        this.__changeDetectorRef.markForCheck();
      });
    this.__subscriptions.add(getExceptionMessagesSubscription);

  }

  public ngOnDestroy(): void {
    this.__subscriptions.unsubscribe();
  }

  //#endregion
}
