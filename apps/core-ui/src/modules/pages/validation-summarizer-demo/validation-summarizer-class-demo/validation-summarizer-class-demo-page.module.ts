import {NgModule} from '@angular/core';
import {ValidationSummarizerClassDemoPageComponent} from './validation-summarizer-class-demo-page.component';
import {ValidationSummarizerClassDemoPageRoutingModule} from './validation-summarizer-class-demo-page-routing.module';
import {ReactiveFormsModule} from '@angular/forms';
import {ValidationSummarizerModule, CommonValidatorModule} from '@ui-tool/core';

@NgModule({
  imports: [
    ValidationSummarizerModule,
    CommonValidatorModule.forChild({
      defaultValidationClasses: ['text-danger'],
      defaultControlValidationClasses: ['border', 'border-danger']
    }),
    ValidationSummarizerClassDemoPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [
    ValidationSummarizerClassDemoPageComponent
  ],
  exports: [
    ValidationSummarizerClassDemoPageComponent
  ]
})
export class ValidationSummarizerClassDemoPageModule {
}
