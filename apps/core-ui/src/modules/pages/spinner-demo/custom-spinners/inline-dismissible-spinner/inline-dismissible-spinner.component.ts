import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'inline-dismissible-spinner',
  templateUrl: 'inline-dismissible-spinner.component.html',
  styleUrls: ['inline-dismissible-spinner.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class InlineDismissibleSpinnerComponent {

}
