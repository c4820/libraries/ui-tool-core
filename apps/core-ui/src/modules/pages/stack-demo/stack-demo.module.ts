import {NgModule} from '@angular/core';
import {StackDemoRoutingModule} from '@ui-demo/core/modules/pages/stack-demo/stack-demo-routing.module';
import {StackDemoComponent} from '@ui-demo/core/modules/pages/stack-demo/stack-demo.component';
import {RouterModule} from '@angular/router';

@NgModule({
  imports: [
    StackDemoRoutingModule,
    RouterModule
  ],
  declarations: [
    StackDemoComponent
  ]
})
export class StackDemoModule {

}
