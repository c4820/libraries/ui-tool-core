import {NgModule} from '@angular/core';
import {InlineDismissibleSpinnerComponent} from './inline-dismissible-spinner.component';

@NgModule({
  declarations: [
    InlineDismissibleSpinnerComponent
  ],
  exports: [
    InlineDismissibleSpinnerComponent
  ]
})
export class InlineDismissibleSpinnerModule {

}
