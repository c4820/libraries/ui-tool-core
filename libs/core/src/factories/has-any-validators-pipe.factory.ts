import {ValidationSummarizerBaseService} from '../services/implementations/validators/validation-summarizers/validation-summarizer-base.service';
import {v4 as uuid} from 'uuid';

// tslint:disable-next-line:typedef
export function buildHasAnyValidatorService() {
  return new ValidationSummarizerBaseService(uuid());
}
