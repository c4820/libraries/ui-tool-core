import {NgModule} from '@angular/core';
import {NavigationBarModule} from '@ui-demo/core/modules/cores/main-layout/navigation-bar/navigation-bar.module';
import {MainLayoutComponent} from '@ui-demo/core/modules/cores/main-layout/main-layout.component';
import {SidebarModule} from '@ui-demo/core/modules/cores/main-layout/sidebar/sidebar.module';
import {RouterModule} from '@angular/router';

@NgModule({
  imports: [NavigationBarModule, SidebarModule, RouterModule],
  declarations: [
    MainLayoutComponent
  ]
})
export class MainLayoutModule {

}
