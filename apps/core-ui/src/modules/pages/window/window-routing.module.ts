import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {WindowComponent} from './window.component';

const routes: Routes = [
  {
    path: '',
    component: WindowComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class WindowRoutingModule {

}
