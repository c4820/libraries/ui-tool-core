import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'body',
  templateUrl: 'app.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AppComponent {

  //#region Properties

  title = 'ui-lib-playground';

  //#endregion
}
