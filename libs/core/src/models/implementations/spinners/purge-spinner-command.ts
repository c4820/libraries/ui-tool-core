import {ISpinnerCommand} from '../../interfaces';
import {SpinnerCommands} from '../../../constants';

export class PurgeSpinnerCommand implements ISpinnerCommand {

  //#region Properties

  public readonly kind: string;

  //#endregion

  //#region Constructor

  public constructor(public readonly containerId: string) {
    this.kind = SpinnerCommands.purge;
  }

  //#endregion

}
