import {NgModule} from '@angular/core';
import {RoleSentinelBasicUsageComponent} from './role-sentinel-basic-usage.component';
import {ReactiveFormsModule} from '@angular/forms';
import {RoleSentinelBasicUsageRoutingModule} from './role-sentinel-basic-usage-routing.module';
import {RoleService} from '../role.service';
import {RoleSentinelModule} from '@ui-tool/core';
import { RoleSentinelService } from '../role-sentinel.service';
import {ROLE_SENTINEL_SERVICE} from '@ui-tool/core';

@NgModule({
  declarations: [
    RoleSentinelBasicUsageComponent
  ],
  imports: [
    RoleSentinelBasicUsageRoutingModule,
    ReactiveFormsModule,
    RoleSentinelModule.forRoot({
      providers: [
        {
          provide: ROLE_SENTINEL_SERVICE,
          useClass: RoleSentinelService
        }
      ]
    })
  ],
  exports: [
    RoleSentinelBasicUsageComponent
  ],
  providers: [
    RoleService
  ]
})
export class RoleSentinelBasicUsageModule {

}
