import {ActivatedRouteSnapshot, CanActivate, RouterStateSnapshot, UrlTree} from '@angular/router';
import {Injectable} from '@angular/core';
import {forkJoin, Observable} from 'rxjs';
import {ApplicationStore} from '@ui-demo/core/modules/cores/stores/application.store';
import {map, tap} from 'rxjs/operators';
import {ApplicationQuery} from '@ui-demo/core/modules/cores/stores/application.query';
import {ApplicationService} from '@ui-demo/core/services/application/application.service';

@Injectable()
export class AppGuard implements CanActivate {

  //#region Constructor

  public constructor(private readonly __applicationQuery: ApplicationQuery,
                     private readonly __applicationStore: ApplicationStore,
                     private readonly __applicationService: ApplicationService) {
  }

  //#endregion

  //#region Methods

  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot)
    : Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {

    const getApplicationMetadata$ = this.__applicationService.getMetaDataAsync();
    return forkJoin([getApplicationMetadata$])
      .pipe(
        tap(([packageMetadata]) => {
          this.__applicationStore.setVersion(packageMetadata.version);
        }),
        map(() => true)
      );
  }

  //#endregion
}
