import {Provider} from '@angular/core';

export class FeatureSentinelOption {

    public providers?: Provider[];

}
