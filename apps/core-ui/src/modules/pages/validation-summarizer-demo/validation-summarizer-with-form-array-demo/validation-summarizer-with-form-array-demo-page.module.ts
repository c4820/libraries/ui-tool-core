import {NgModule} from '@angular/core';
import {ValidationSummarizerWithFormArrayDemoPageComponent} from './validation-summarizer-with-form-array-demo-page.component';
import {ValidationSummarizerWithFormArrayDemoPageRoutingModule} from './validation-summarizer-with-form-array-demo-page-routing.module';
import {ReactiveFormsModule} from '@angular/forms';
import {ValidationSummarizerModule, CommonValidatorModule, WindowAccessorModule} from '@ui-tool/core';
import {CommonModule} from '@angular/common';
import {ToAnyPipeModule} from '@ui-demo/core/pipes/to-any/to-any-pipe.module';

@NgModule({
  imports: [
    WindowAccessorModule,
    ValidationSummarizerModule,
    CommonValidatorModule.forChild({
      defaultValidationClasses: ['text-danger'],
      defaultControlValidationClasses: ['border', 'border-danger']
    }),
    ValidationSummarizerWithFormArrayDemoPageRoutingModule,
    ReactiveFormsModule,
    CommonModule,
    ToAnyPipeModule
  ],
  declarations: [
    ValidationSummarizerWithFormArrayDemoPageComponent
  ],
  exports: [
    ValidationSummarizerWithFormArrayDemoPageComponent
  ]
})
export class ValidationSummarizerWithFormArrayDemoPageModule {
}
