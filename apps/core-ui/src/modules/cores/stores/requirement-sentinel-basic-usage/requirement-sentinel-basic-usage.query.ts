import {Query} from '@datorama/akita';
import {
  IRequirementSentinelBasicUsageState
} from '@ui-demo/core/modules/cores/stores/requirement-sentinel-basic-usage/requirement-sentinel-basic-usage.state';
import {Injectable} from '@angular/core';
import {
  RequirementSentinelBasicUsageStore
} from '@ui-demo/core/modules/cores/stores/requirement-sentinel-basic-usage/requirement-sentinel-basic-usage.store';

@Injectable({
  providedIn: 'root'
})
export class RequirementSentinelBasicUsageQuery extends Query<IRequirementSentinelBasicUsageState> {

  //#region Properties

  public readonly usageOneEnabled$ = this.select(m => m.usageOneEnabled);
  public readonly usageTwoEnabled$ = this.select(m => m.usageTwoEnabled);

  //#endregion

  //#region Constructor

  public constructor(store: RequirementSentinelBasicUsageStore) {
    super(store);
  }

  //#endregion
}
