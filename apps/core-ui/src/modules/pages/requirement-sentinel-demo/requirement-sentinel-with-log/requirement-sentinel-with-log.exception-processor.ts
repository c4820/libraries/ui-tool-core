import {
  IRequirementSentinelExceptionProcessor, RequirementSentinelExceptionTypes
} from '@ui-tool/core';
import {Injectable} from '@angular/core';
import {
  IRequirementSentinelException
} from '@ui-tool/core';
import {Observable, of} from 'rxjs';
import {
  RequirementSentinelWithLogStore
} from '@ui-demo/core/modules/cores/stores/requirement-sentinel-with-log/requirement-sentinel-with-log.store';

@Injectable()
export class RequirementSentinelWithLogExceptionProcessor implements IRequirementSentinelExceptionProcessor {

  //#region Constructor

  public constructor(private readonly __store: RequirementSentinelWithLogStore) {
  }

  //#endregion

  //#region Methods

  public processExceptionAsync<T>(instance: IRequirementSentinelException<T>): Observable<void> {
    console.log(`Dont be afraid, the below lines are exception logs only`);
    console.error(instance);
    switch (instance.type) {
      case RequirementSentinelExceptionTypes.INTERNAL:
        this.__store.update(state => ({
          ...state,
          internalExceptions: state.internalExceptions.concat([instance])
        }));
        break;

      default:
        this.__store.update(state => ({
          ...state,
          externalExceptions: state.externalExceptions.concat([instance])
        }));
        break;
    }
    return of(void(0));
  }

  //#endregion
}
