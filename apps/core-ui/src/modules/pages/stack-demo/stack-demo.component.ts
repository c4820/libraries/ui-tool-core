import {ChangeDetectionStrategy, Component} from '@angular/core';

@Component({
  selector: 'stack-demo',
  templateUrl: './stack-demo.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StackDemoComponent {

}
