import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RoleSentinelWithElseTemplateComponent} from './role-sentinel-with-else-template.component';

const routes: Routes = [
  {
    path: '',
    component: RoleSentinelWithElseTemplateComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class RoleSentinelWithElseTemplateRoutingModule {

}
