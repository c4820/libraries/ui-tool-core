import {NgModule} from '@angular/core';
import {StackDemoBasicUsageComponent} from './stack-demo-basic-usage.component';
import {RouterModule} from '@angular/router';
import {CommonModule} from '@angular/common';
import {StackServiceModule} from '@ui-tool/core';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: StackDemoBasicUsageComponent
            }
        ]),
        CommonModule,
      StackServiceModule.forRoot()
    ],
  declarations: [
    StackDemoBasicUsageComponent
  ],
  exports: [
    StackDemoBasicUsageComponent
  ]
})
export class StackDemoBasicUsageModule {

}
