import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RequirementSentinelWithLogComponent} from './requirement-sentinel-with-log.component';

const routes: Routes = [
  {
    path: '',
    component: RequirementSentinelWithLogComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class RequirementSentinelWithLogRoutingModule {

}
