import {NgModule} from '@angular/core';
import {WindowAccessorModule} from '@ui-tool/core';
import {WindowComponent} from './window.component';
import {WindowRoutingModule} from './window-routing.module';

@NgModule({
  imports: [
    WindowRoutingModule,
    WindowAccessorModule
  ],
  declarations: [
    WindowComponent
  ],
  exports: [
    WindowComponent
  ]
})
export class WindowModule {

}
