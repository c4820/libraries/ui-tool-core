export class AuthenticationService {

  //#region Properties

  private __features: string[];

  //#endregion

  //#region Constructor

  public constructor() {
    this.__features = [];
  }

  //#endregion

  //#region Methods

  public updateFeatures(features: string[]): void {
    this.__features = features || [];
  }

  public getFeatures(): string[] {
    return this.__features;
  }

  //#endregion

}
