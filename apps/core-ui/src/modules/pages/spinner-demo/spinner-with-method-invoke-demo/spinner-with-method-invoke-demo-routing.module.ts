import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {SpinnerWithMethodInvokeDemoComponent} from './spinner-with-method-invoke-demo.component';

const routes: Routes = [
  {
    path: '',
    component: SpinnerWithMethodInvokeDemoComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class SpinnerWithMethodInvokeDemoRoutingModule {

}
