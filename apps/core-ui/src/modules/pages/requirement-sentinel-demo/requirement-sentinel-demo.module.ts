import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'with-else-template',
        loadChildren: () => import('./requirement-sentinel-else-template/requirement-sentinel-else-template.module')
          .then(m => m.RequirementSentinelElseTemplateModule)
      },
      {
        path: 'with-log',
        loadChildren: () => import('./requirement-sentinel-with-log/requirement-sentinel-with-log.module')
          .then(m => m.RequirementSentinelWithLogModule)
      },
      {
        path: 'with-context',
        loadChildren: () => import('./requirement-sentinel-with-context/requirement-sentinel-with-context.module')
          .then(m => m.RequirementSentinelWithContextModule)
      },
      {
        path: 'with-child-modules',
        loadChildren: () => import('./requirement-sentinel-with-child-modules/requirement-sentinel-with-child-modules.module')
          .then(m => m.RequirementSentinelWithChildModulesModule)
      },
      {
        path: '',
        loadChildren: () => import('./requirement-sentinel-basic-usage/requirement-sentinel-basic-usage.module')
          .then(m => m.RequirementSentinelBasicUsageModule)
      },
    ])
  ]
})
export class RequirementSentinelDemoModule {

}
