import {ChangeDetectionStrategy, Component, Inject} from '@angular/core';
import {WINDOW} from '@ui-tool/core';

@Component({
  selector: 'window-component',
  templateUrl: 'window.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class WindowComponent {

  //#region Constructor

  public constructor(@Inject(WINDOW) protected readonly _window: Window) {
  }

  //#endregion

  //#region Methods

  public showAlert(): void {
    this._window.alert('This is alert message');
  }

  public showConfirmation(): void {
    if (this._window.confirm('Are you sure to close this confirm message ?')) {
      this._window.alert('Sadly, you chose YES');
      return;
    }

    this._window.alert('Yay, you chose CANCEL');
  }

  //#endregion

}
