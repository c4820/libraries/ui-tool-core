import {ChangeDetectionStrategy, Component, Inject} from '@angular/core';
import {SPINNER_METHOD_INVOKE_CALLBACK} from '@ui-tool/core';

@Component({
  selector: 'inline-dismissable-spinner',
  templateUrl: 'method-invoke-spinner.component.html',
  styleUrls: ['method-invoke-spinner.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class MethodInvokeSpinnerComponent {

  //#region Constructor

  public constructor(@Inject(SPINNER_METHOD_INVOKE_CALLBACK)
                     protected readonly _methodInvokeCallback: (methodName: string, payload: any) => void) {
  }

  //#endregion

  //#region Methods

  public invoke(name: string, payload?: any): void {
    this._methodInvokeCallback?.(name, payload);
  }

  public invokeClose(): void {
    this.invoke('close');
  }

  //#endregion
}
