import {IValidationSummarizerModuleOptions} from './validation-summarizer-module-options.interface';

export interface ICommonValidatorModuleOptions extends IValidationSummarizerModuleOptions {

  //#region Properties

  // Classes which is used when validation classes must be applied to a control.
  defaultControlValidationClasses?: string[] | null;

  // Validation classes which is used when validation classes must be applied to an element.
  defaultValidationClasses?: string[] | null;

  //#endregion
}
