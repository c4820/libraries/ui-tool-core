import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RequirementSentinelChildModule1Component} from './requirement-sentinel-child-module-1.component';

const routes: Routes = [
  {
    path: '',
    component: RequirementSentinelChildModule1Component
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class RequirementSentinelChildModule1RoutingModule {

}
