import {ISpinnerCommand} from '../../interfaces';
import {SpinnerCommands} from '../../../constants/spinner-commands';
import {DisplaySpinnerOptions} from './display-spinner-options';

export class DisplaySpinnerCommand implements ISpinnerCommand {

  //#region Properties

  public readonly kind: string;

  //#endregion

  //#region Constructor

  public constructor(public containerId: string,
                     public id: string,
                     public options?: DisplaySpinnerOptions) {
    this.kind = SpinnerCommands.display;
  }

  //#endregion

}
