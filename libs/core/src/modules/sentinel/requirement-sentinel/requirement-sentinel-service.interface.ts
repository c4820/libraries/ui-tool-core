import {Observable} from 'rxjs';

export interface IMeetRequirementService {

  //#region Methods

  // Trigger validation on directives.
  doValidation(): void;

  // Register control with validation event.
  hookValidationEventAsync(): Observable<void>;

  // Whether requirement should be met or not.
  shouldRequirementMetAsync<TContext>(name: string, context?: object): Observable<boolean>;

  //#endregion
}
