import {ChangeDetectorRef, Directive, ElementRef, Inject, Input, OnDestroy, OnInit} from '@angular/core';
import {AbstractControl, FormControlDirective, FormControlName, NgControl, NgModel} from '@angular/forms';
import {Observable} from 'rxjs';
import {
  COMMON_VALIDATOR_OPTIONS_PROVIDER,
  COMMON_VALIDATOR_SERVICE
} from '../../../../constants/injectors/common-validator-injectors';
import {
  IValidationSummarizerService
} from '../../../../services/interfaces/validation-summarizers/validation-summarizer-service.interface';
import {ValidatorClassBase} from './validator-class-base';
import {ICommonValidatorModuleOptionProvider} from '../../../../providers';

@Directive({
  selector: '[with-invalid-validator-class][ngModel], [with-invalid-validator-class][formControl], [with-invalid-validator-class][formControlName]'
})
export class WithInvalidValidatorClassInFormControlDirective extends ValidatorClassBase implements OnInit, OnDestroy {

  //#region Properties

  //#endregion

  //#region Accessors

  @Input('with-invalid-validator-class')
  public set classes(value: string[] | string) {
    super.classes = value;
  }

  //#endregion

  //#region Constructor

  public constructor(@Inject(COMMON_VALIDATOR_SERVICE)
                     protected readonly _validationSummarizerService: IValidationSummarizerService,
                     @Inject(COMMON_VALIDATOR_OPTIONS_PROVIDER)
                     protected readonly _optionProvider: ICommonValidatorModuleOptionProvider,
                     protected readonly _changeDetectorRef: ChangeDetectorRef,
                     protected readonly _elementRef: ElementRef,
                     protected readonly ngControl: NgControl) {
    super(_validationSummarizerService, _changeDetectorRef, _elementRef);
  }

  //#endregion

  //#region Methods

  public ngOnInit(): void {

    if (this.ngControl instanceof FormControlName) {
      this._control = (this.ngControl as FormControlName).control;
    } else if (this.ngControl instanceof FormControlDirective) {
      this._control = (this.ngControl as FormControlDirective).control;
    } else if (this.ngControl instanceof NgModel) {
      this._control = (this.ngControl as NgModel).control;
    }

    if (this._control) {
      this.hookControlStatusChanges(this._control);
    }
  }

  public ngOnDestroy(): void {
    super.ngOnDestroy();
  }

  //#endregion

  //#region Methods

  protected hookControlStatusChanges(control: NgControl | AbstractControl): void {

    // Unsubscribe the previous subscription.
    this._hookStatusChangesSubscription?.unsubscribe();
    this._control = control;

    let statusChangesObservable: Observable<any> | null = null;
    if (control instanceof AbstractControl) {
      statusChangesObservable = control.statusChanges;
    } else if (control instanceof NgControl) {
      statusChangesObservable = control.statusChanges;
    }

    this._hookStatusChangesSubscription = statusChangesObservable?.subscribe(() => {
      this.buildElementClasses(control);
    });
  }

  protected getValidationClasses(): string[] {
    if (this._classes && this._classes.length) {
      return this._classes;
    }

    return this._optionProvider
      .getOption().defaultControlValidationClasses || [];
  }

  //#endregion

}
