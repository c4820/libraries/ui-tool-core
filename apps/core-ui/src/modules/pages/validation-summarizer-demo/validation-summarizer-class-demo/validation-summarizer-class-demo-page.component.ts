import {ChangeDetectionStrategy, Component, Inject} from '@angular/core';
import {ValidationSummarizerClassDemoPageFields} from './validation-summarizer-class-demo-page-fields';
import {FormGroup} from '@angular/forms';
import {VALIDATION_SUMMARIZER_SERVICE, IValidationSummarizerService} from '@ui-tool/core';
@Component({
  selector: 'validation-summarizer-class-demo-page',
  templateUrl: 'validation-summarizer-class-demo-page.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ValidationSummarizerClassDemoPageComponent {

  //#region Properties

  private readonly __fields: ValidationSummarizerClassDemoPageFields;

  public readonly controlNames = ValidationSummarizerClassDemoPageFields;

  //#endregion

  //#region Accessors

  public get formGroup(): FormGroup {
    return this.__fields.toFormGroup();
  }

  //#endregion

  //#region Constructor

  public constructor(@Inject(VALIDATION_SUMMARIZER_SERVICE)
                     protected readonly _validationSummarizerService: IValidationSummarizerService) {
    this.__fields = new ValidationSummarizerClassDemoPageFields();
  }

  //#endregion

  //#region Methods

  public doValidation(): void {
    this._validationSummarizerService.doFormControlsValidation(this.formGroup);
  }

  //#endregion
}
