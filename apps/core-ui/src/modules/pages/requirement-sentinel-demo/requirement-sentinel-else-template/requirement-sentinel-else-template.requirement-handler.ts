import {IRequirementHandler} from '@ui-tool/core';
import {Injectable} from '@angular/core';
import {Observable} from 'rxjs';
import {
  RequirementSentinelBasicUsageQuery
} from '@ui-demo/core/modules/cores/stores/requirement-sentinel-basic-usage/requirement-sentinel-basic-usage.query';
import {
  RequirementSentinelBasicUsageNames
} from '@ui-demo/core/modules/pages/requirement-sentinel-demo/requirement-sentinel-basic-usage/requirement-sentinel-basic-usage-names';
import {
  RequirementSentinelElseTemplateNames
} from '@ui-demo/core/modules/pages/requirement-sentinel-demo/requirement-sentinel-else-template/requirement-sentinel-else-template-names';
import {
  RequirementSentinelElseTemplateQuery
} from '@ui-demo/core/modules/cores/stores/requirement-sentinel-else-template/requirement-sentinel-else-template.query';

@Injectable()
export class RequirementSentinelElseTemplateRequirementHandler implements IRequirementHandler {

  //#region Properties

  public readonly name: string;

  //#endregion

  //#region Constructor

  public constructor(private readonly __query: RequirementSentinelElseTemplateQuery) {
    this.name = RequirementSentinelElseTemplateNames.DEFAULT;
  }

  //#endregion

  //#region Methods

  public shouldRequirementMetAsync(): Observable<boolean> {
    return this.__query.requirementSatisfied$;
  }

  //#endregion
}
