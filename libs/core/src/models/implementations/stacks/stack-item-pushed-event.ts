import {IStackItemEvent} from '../../interfaces';
import {StackItemEvents} from '../../../constants';

export class StackItemPushedEvent implements IStackItemEvent {

  //#region Properties

  public readonly kind: string;

  //#endregion

  //#region Constructor

  public constructor(
    public readonly stackCode: string,
    public readonly id: string,
    public readonly itemIds: string[]) {
    this.kind = StackItemEvents.ITEM__PUSHED;
    this.stackCode = stackCode;
    this.id = id;
  }

  //#endregion


}
