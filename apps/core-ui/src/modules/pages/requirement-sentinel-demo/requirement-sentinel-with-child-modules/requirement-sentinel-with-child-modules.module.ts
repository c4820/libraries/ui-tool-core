import {NgModule} from '@angular/core';
import {RequirementSentinelWithChildModulesComponent} from './requirement-sentinel-with-child-modules.component';
import {RequirementSentinelWithChildModulesRoutingModule} from './requirement-sentinel-with-child-modules-routing.module';
import {REQUIREMENT_HANDLER, RequirementSentinelModule} from '@ui-tool/core';
import {ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {RouterModule} from '@angular/router';
import {
  RequirementSentinelWithChildModulesParentRequirementHandler
} from '@ui-demo/core/modules/pages/requirement-sentinel-demo/requirement-sentinel-with-child-modules/requirement-sentinel-with-child-modules-parent.requirement-handler';

@NgModule({
  imports: [
    RequirementSentinelModule.forRoot(),
    RequirementSentinelWithChildModulesRoutingModule,
    ReactiveFormsModule,
    CommonModule,
    RouterModule
  ],
  declarations: [
    RequirementSentinelWithChildModulesComponent
  ],
  providers: [
    {
      provide: REQUIREMENT_HANDLER,
      useClass: RequirementSentinelWithChildModulesParentRequirementHandler,
      multi: true
    },
  ]
})
export class RequirementSentinelWithChildModulesModule {

}
