import {Injector, Pipe, PipeTransform} from '@angular/core';
import {ISmartNavigatorService} from '../../services/interfaces/smart-navigator-service.interface';
import {SMART_NAVIGATOR_SERVICE} from '../../constants/injectors/injectors';
import {NavigationExtras, UrlTree} from '@angular/router';
import {NavigateToScreenRequest} from '../../models/implementations/smart-navigators/navigate-to-screen-request';

@Pipe({
  name: 'toUrlTree'
})
export class ToUrlTreePipe implements PipeTransform {

  //#region Services

  protected readonly _navigatorService: ISmartNavigatorService;

  //#endregion

  //#region Constructor

  public constructor(injector: Injector) {
    this._navigatorService = injector.get(SMART_NAVIGATOR_SERVICE);
  }


  //#endregion

  //#region Methods

  public transform(value: string | NavigateToScreenRequest<any>, routeParams?: { [key: string]: any; },
                   extras?: NavigationExtras): UrlTree {

    if (extras) {
      extras.queryParams = null;
    }

    if (typeof(value) === 'string') {
      return this._navigatorService.buildUrlTree(value, routeParams, extras);
    }

    return this._navigatorService.buildUrlTreeFromNavigationRequest(value);
  }

  //#endregion

}
