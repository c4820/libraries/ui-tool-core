export class StackItemOptions {

  //#region Properties

  // Whether previous item must be destroyed or not.
  public purge?: boolean;

  public onItemBeingRemoved?: () => boolean;

  //#endregion

}
