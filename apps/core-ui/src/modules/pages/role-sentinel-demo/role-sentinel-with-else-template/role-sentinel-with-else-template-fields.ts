import {FormControl, FormGroup} from '@angular/forms';
import {UserRoles} from '../user-roles';

export class RoleSentinelWithElseTemplateFields {

  //#region Properties

  private readonly __formGroup: FormGroup;

  //#endregion

  //#region Accessors

  public get formGroup(): FormGroup {
    return this.__formGroup;
  }

  //#endregion

  //#region Constructor

  public constructor() {
    this.__formGroup = new FormGroup({
      [UserRoles.admin]: new FormControl(false),
      [UserRoles.operator]: new FormControl(false),
      [UserRoles.contentWriter]: new FormControl(false)
    });
  }

  //#endregion

}
