import {IRequirementSentinelException} from '@ui-tool/core';

export interface IRequirementSentinelWithLogState {
  //#region Properties

  internalExceptions: IRequirementSentinelException<any>[];
  externalExceptions: IRequirementSentinelException<any>[];

  //#endregion
}
