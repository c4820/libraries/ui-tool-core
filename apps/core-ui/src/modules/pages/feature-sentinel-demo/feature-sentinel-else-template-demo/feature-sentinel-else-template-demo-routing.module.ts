import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FeatureSentinelElseTemplateDemoComponent} from './feature-sentinel-else-template-demo.component';

const routes: Routes = [
  {
    path: '',
    component: FeatureSentinelElseTemplateDemoComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class FeatureSentinelElseTemplateDemoRoutingModule {

}
