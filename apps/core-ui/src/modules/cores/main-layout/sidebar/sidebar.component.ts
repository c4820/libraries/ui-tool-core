import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import { ScreenCodes } from '@ui-demo/core/constants/screen-codes';
import {ApplicationQuery} from '@ui-demo/core/modules/cores/stores/application.query';
import {Subscription} from 'rxjs';
import {map} from 'rxjs/operators';
import {BasicSpinnerUsageNavigationRequest} from '@ui-demo/core/models/navigation-requests/basic-spinner-usage-navigation-request';

@Component({
  selector: 'sidebar',
  templateUrl: 'sidebar.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SidebarComponent implements OnInit, OnDestroy {

  //#region Properties

  public readonly basicSpinnerUsageNavigationRequest = new BasicSpinnerUsageNavigationRequest();

  public readonly ScreenCodes = ScreenCodes;

  private __version?: string;

  private readonly __subscription: Subscription;

  //#endregion

  //#region Accessors

  public get version(): string | undefined {
    return this.__version;
  }

  //#endregion

  //#region Constructor

  public constructor(private readonly __applicationQuery: ApplicationQuery,
                     private readonly __changeDetectorRef: ChangeDetectorRef) {
    this.__subscription = new Subscription();
  }

  //#endregion

  //#region Life cycle hooks

  public ngOnInit(): void {
    const getVersionSubscription = this.__applicationQuery.version$
      .subscribe(version => {
        this.__version = version;
        this.__changeDetectorRef.markForCheck();
      });
    this.__subscription.add(getVersionSubscription);
  }

  public ngOnDestroy(): void {
    this.__subscription?.unsubscribe();
  }

  //#endregion


}
