import {NgModule} from '@angular/core';
import {SpinnerDemoRoutingModule} from './spinner-demo-routing.module';
import {RouterModule} from '@angular/router';
import {SpinnerContainerModule} from '@ui-tool/core';

@NgModule({
  imports: [
    SpinnerContainerModule.forRoot(),
    SpinnerDemoRoutingModule,
    RouterModule
  ]
})
export class SpinnerDemoModule {

}
