export class UserRoles {

  //#region Properties

  public static readonly admin = 'admin';

  public static readonly operator = 'operator';

  public static readonly contentWriter = 'contentWriter';

  //#endregion

}
