import {IRequirementHandler} from '@ui-tool/core';
import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {
  RequirementSentinelWithLogNames
} from '@ui-demo/core/modules/pages/requirement-sentinel-demo/requirement-sentinel-with-log/requirement-sentinel-with-log-names';
import {
  RequirementSentinelWithLogQuery
} from '@ui-demo/core/modules/cores/stores/requirement-sentinel-with-log/requirement-sentinel-with-log.query';

@Injectable()
export class RequirementSentinelWithLogRequirementHandler implements IRequirementHandler {

  //#region Properties

  public readonly name: string;

  //#endregion

  //#region Constructor

  public constructor(private readonly __query: RequirementSentinelWithLogQuery) {
    this.name = RequirementSentinelWithLogNames.DEFAULT;
  }

  //#endregion

  //#region Methods

  public shouldRequirementMetAsync(): Observable<boolean> {
    return of(true);
  }

  //#endregion
}
