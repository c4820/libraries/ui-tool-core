import {ChangeDetectionStrategy, Component, Inject} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {BasicValidationSummarizerDemoFields} from './basic-validation-summarizer-demo-fields';
import {IValidationSummarizerService, VALIDATION_SUMMARIZER_SERVICE} from '@ui-tool/core';

@Component({
  selector: 'basic-validation-summarizer-demo-demo',
  templateUrl: 'basic-validation-summarizer-demo.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BasicValidationSummarizerDemoComponent {

  //#region Properties

  private readonly __formGroup: FormGroup;

  //#endregion

  //#region Accessors

  public get formGroup(): FormGroup {
    return this.__formGroup;
  }

  //#endregion

  //#region Accessors

  public get controlNames(): typeof BasicValidationSummarizerDemoFields {
    return BasicValidationSummarizerDemoFields;
  }

  //#endregion

  //#region Constructor

  public constructor(@Inject(VALIDATION_SUMMARIZER_SERVICE)
                     protected readonly _validationSummarizerService: IValidationSummarizerService) {
    this.__formGroup = new BasicValidationSummarizerDemoFields().toFormGroup();
  }

  //#endregion

  //#region Methods

  public clickDoValidation(): void {
    this._validationSummarizerService.doFormControlsValidation(this.formGroup);
  }

  //#endregion

}
