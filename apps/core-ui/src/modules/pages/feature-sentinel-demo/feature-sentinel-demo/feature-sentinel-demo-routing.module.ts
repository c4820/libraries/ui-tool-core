import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {FeatureSentinelDemoComponent} from './feature-sentinel-demo.component';

const routes: Routes = [
  {
    path: '',
    component: FeatureSentinelDemoComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class FeatureSentinelDemoRoutingModule {

}
