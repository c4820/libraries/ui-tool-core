import {ISpinnerService} from '../../interfaces';
import {Observable, ReplaySubject, Subject} from 'rxjs';
import {DisplaySpinnerCommand} from '../../../models/implementations/spinners/display-spinner-command';
import {v4 as uuidv4} from 'uuid';
import {DeleteSpinnerCommand} from '../../../models/implementations/spinners/delete-spinner-command';
import {SpinnerCommands} from '../../../constants';
import {DisplaySpinnerOptions} from '../../../models/implementations/spinners/display-spinner-options';
import {ISpinnerCommand} from '../../../models/interfaces/spinners/spinner-command.interface';

export class SpinnerService implements ISpinnerService {

  //#region Properties

  // Mapping between container id & visibility emitter.
  private readonly _containerIdToVisibilityEvent: {
    [containerId: string]
      : Subject<ISpinnerCommand>
  };

  // Container ids which already registered.
  private readonly _containerIds: string[];

  //#endregion

  //#region Constructor

  public constructor() {
    this._containerIdToVisibilityEvent = {};
    this._containerIds = [];
  }

  //#endregion

  //#region Methods

  public displaySpinner(containerId: string, options?: DisplaySpinnerOptions): string {

    // Initialize request id.
    const requestId = uuidv4();

    const displaySpinnerRequest = new DisplaySpinnerCommand(containerId, requestId, options);
    this._sendSpinnerCommand(displaySpinnerRequest);
    return requestId;
  }

  public deleteSpinner(containerId: string, id: string): void {

    // Delete spinner request.
    const deleteSpinnerRequest = new DeleteSpinnerCommand(containerId, id);

    // Deleted requests is smaller than container request.
    this._sendSpinnerCommand(deleteSpinnerRequest);
  }

  public deleteSpinners(containerId?: string): void {

    if (!containerId) {

      for (const displayedSpinnerId of this._containerIds) {
        const deleteSpinnerRequest = new DeleteSpinnerCommand(displayedSpinnerId);
        this._sendSpinnerCommand(deleteSpinnerRequest);
      }

      this._containerIds.splice(0);
      return;
    }

    this._sendSpinnerCommand(new DeleteSpinnerCommand(containerId));
  }

  // Hook visibility changed event.
  public hookSpinnerVisibilityEvent(containerId: string): Observable<DisplaySpinnerCommand | DeleteSpinnerCommand> {

    if (!this._containerIdToVisibilityEvent[containerId]) {
      this._containerIdToVisibilityEvent[containerId] = new ReplaySubject(1);
    }

    return this._containerIdToVisibilityEvent[containerId].asObservable();
  }

  //#endregion

  //#region Internal methods

  // Broadcast visibility event.
  protected _sendSpinnerCommand(command: ISpinnerCommand): void {

    if (!command || !command.containerId) {
      return;
    }

    if (!this._containerIdToVisibilityEvent[command.containerId]) {
      this._containerIdToVisibilityEvent[command.containerId] = new ReplaySubject<ISpinnerCommand>(1);
    }

    if (command.kind === SpinnerCommands.display) {
      const displaySpinnerRequest = command as DisplaySpinnerCommand;
      const itemIndex = this._containerIds.indexOf(displaySpinnerRequest.containerId);
      if (itemIndex < 0) {
        this._containerIds.push(displaySpinnerRequest.containerId);
      }
    }

    this._containerIdToVisibilityEvent[command.containerId].next(command);
  }

  //#endregion

}
