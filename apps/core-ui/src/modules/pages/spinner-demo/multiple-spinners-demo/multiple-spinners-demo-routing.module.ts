import {RouterModule, Routes} from '@angular/router';
import {MultipleSpinnersDemoComponent} from './multiple-spinners-demo.component';
import {NgModule} from '@angular/core';

const routes: Routes = [
  {
    path: '',
    component: MultipleSpinnersDemoComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class MultipleSpinnersDemoRoutingModule {
}
