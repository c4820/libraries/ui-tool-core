import {IFeatureSentinelService} from '@ui-tool/core';
import {Inject, Injectable} from '@angular/core';
import {Observable, of, Subject} from 'rxjs';
import {catchError, map} from 'rxjs/operators';
import {AuthenticationService} from '../authentication.service';

@Injectable()
export class FeatureSentinelService implements IFeatureSentinelService {
  //#region Properties

  private readonly _doValidationEvent: Subject<void>;

  //#endregion

  //#region Constructor

  public constructor(protected readonly _authenticationService: AuthenticationService) {
    this._doValidationEvent = new Subject();
  }

  //#endregion

  //#region Methods

  public doValidation(): void {
    this._doValidationEvent.next();
  }

  public hookValidationEventAsync(): Observable<void> {
    return this._doValidationEvent.asObservable();
  }

  public ableToAccessFeaturesAsync(designatedFeatures: string[]): Observable<boolean> {

    if (!designatedFeatures || !designatedFeatures.length) {
      return of(true);
    }

    return of(this._authenticationService.getFeatures())
      .pipe(
        map((assignedFeatures: string[]) => {

          const itemIndex = assignedFeatures
            .findIndex(assignedFeature => {
              let assignedFeatureRegex: RegExp;

              if (assignedFeature === '*') {
                assignedFeatureRegex = new RegExp(`^.*$`);
              } else {
                assignedFeatureRegex = new RegExp(`^${assignedFeature}$`);
              }

              return designatedFeatures
                .findIndex(designatedFeature => assignedFeatureRegex.test(designatedFeature)) !== -1;
            });
          return itemIndex !== -1;
        }),
        catchError(() => of(false))
      );
  }

  //#endregion
}
