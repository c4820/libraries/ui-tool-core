import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RoleSentinelBasicUsageComponent} from './role-sentinel-basic-usage.component';

const routes: Routes = [
  {
    path: '',
    component: RoleSentinelBasicUsageComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class RoleSentinelBasicUsageRoutingModule {

}
