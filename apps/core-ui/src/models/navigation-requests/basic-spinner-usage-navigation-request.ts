import {NavigateToScreenRequest} from '@ui-tool/core';
import {ScreenCodes} from '@ui-demo/core/constants/screen-codes';

export class BasicSpinnerUsageNavigationRequest extends NavigateToScreenRequest<void> {

  //#region Constructor

  public constructor() {
    super(ScreenCodes.SPINNER__BASIC_USAGE);
  }

  //#endregion

}
