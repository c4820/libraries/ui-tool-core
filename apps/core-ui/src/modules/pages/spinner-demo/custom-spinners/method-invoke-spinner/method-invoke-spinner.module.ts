import {NgModule} from '@angular/core';
import {MethodInvokeSpinnerComponent} from './method-invoke-spinner.component';

@NgModule({
  declarations: [
    MethodInvokeSpinnerComponent
  ],
  exports: [
    MethodInvokeSpinnerComponent
  ]
})
export class MethodInvokeSpinnerModule {

}
