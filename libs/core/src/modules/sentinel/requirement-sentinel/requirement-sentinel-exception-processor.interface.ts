import {Observable} from 'rxjs';
import {IRequirementSentinelException} from './interfaces/requirement-sentinel-exception.interface';

export interface IRequirementSentinelExceptionProcessor {

  //#region Methods

  processExceptionAsync<T>(instance: IRequirementSentinelException<T>): Observable<void>;

  //#endregion

}
