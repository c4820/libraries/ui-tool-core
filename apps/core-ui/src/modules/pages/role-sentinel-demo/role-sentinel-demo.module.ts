import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'with-else-template',
        loadChildren: () => import('./role-sentinel-with-else-template/role-sentinel-with-else-template.module')
          .then(m => m.RoleSentinelWithElseTemplateModule)
      },
      {
        path: '',
        loadChildren: () => import('./role-sentinel-basic-usage/role-sentinel-basic-usage.module')
          .then(m => m.RoleSentinelBasicUsageModule)
      }
    ])
  ]
})
export class RoleSentinelDemoModule {

}
