import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {StackDemoComponent} from '@ui-demo/core/modules/pages/stack-demo/stack-demo.component';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        component: StackDemoComponent,
        loadChildren: () => import('./stack-demo-basic-usage/stack-demo-basic-usage.module')
          .then(m => m.StackDemoBasicUsageModule)
      }
    ])
  ]
})
export class StackDemoRoutingModule {

}
