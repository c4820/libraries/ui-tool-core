import {NgModule} from '@angular/core';
import {MultipleValidationSummarizerBasicUsageComponent} from './multiple-validation-summarizer-basic-usage.component';
import {ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';
import {TranslateModule} from '@ngx-translate/core';
import {CommonModule} from '@angular/common';
import {CommonValidatorModule, MultipleValidationSummarizerModule, HasAnyValidatorsModule} from '@ui-tool/core';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: MultipleValidationSummarizerBasicUsageComponent
            }
        ]),
        MultipleValidationSummarizerModule.forRoot(),
        CommonValidatorModule.forRoot(),
        ReactiveFormsModule,
        TranslateModule.forChild(),
        CommonModule,
        HasAnyValidatorsModule.forRoot(),
        HasAnyValidatorsModule
    ],
  declarations: [
    MultipleValidationSummarizerBasicUsageComponent
  ],
  exports: [
    MultipleValidationSummarizerBasicUsageComponent
  ]
})
export class MultipleValidationSummarizerBasicUsageModule {

}
