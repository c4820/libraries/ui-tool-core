export class RoleService {

  //#region Properties

  private __roles: string[];

  //#endregion

  //#region Constructor

  public constructor() {
    this.__roles = [];
  }

  //#endregion

  //#region Methods

  public updateRoles(features: string[]): void {
    this.__roles = features || [];
  }

  public getRoles(): string[] {
    return this.__roles;
  }

  //#endregion

}
