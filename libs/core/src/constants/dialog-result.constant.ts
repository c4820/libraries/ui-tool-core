export class DialogResultConstant {

  //#region Properties

  public static readonly reject = 'DISMISS';

  public static readonly resolve = 'MANUALLY_CLOSED';

  //#endregion

}
