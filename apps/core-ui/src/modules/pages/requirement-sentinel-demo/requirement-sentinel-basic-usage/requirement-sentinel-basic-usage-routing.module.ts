import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RequirementSentinelBasicUsageComponent} from './requirement-sentinel-basic-usage.component';

const routes: Routes = [
  {
    path: '',
    component: RequirementSentinelBasicUsageComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class RequirementSentinelBasicUsageRoutingModule {

}
