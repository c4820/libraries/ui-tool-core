import {ISpinnerCommand} from '../../interfaces';
import {SpinnerCommands} from '../../../constants/spinner-commands';

export class DeleteSpinnerCommand implements ISpinnerCommand {

  //#region Properties

  public readonly kind: string;

  // Request to be deleted.
  public id?: string;

  // Id of container whose spinner must be deleted.
  public containerId: string;

  //#endregion

  //#region Constructor

  public constructor(containerId: string, id?: string) {
    this.kind = SpinnerCommands.close;
    this.containerId = containerId;
    this.id = id;
  }

  //#endregion

}
