export class Account {

  //#region Properties

  public name: string;

  public password: string;

  //#endregion

  //#region Constructor

  public constructor() {
    this.name = '';
    this.password = '';
  }

  //#endregion

}
