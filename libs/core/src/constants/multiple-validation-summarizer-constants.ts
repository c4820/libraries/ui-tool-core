// Update event property.
export declare type MULTIPLE_VALIDATION_SUMMARIZER_PROPERTY = 'instance' | 'label' | 'template';
export declare type MULTIPLE_VALIDATION_SUMMARIZER_CONTEXT_CHANGED_EVENT = {
  name: MULTIPLE_VALIDATION_SUMMARIZER_PROPERTY,
  value: any
};
