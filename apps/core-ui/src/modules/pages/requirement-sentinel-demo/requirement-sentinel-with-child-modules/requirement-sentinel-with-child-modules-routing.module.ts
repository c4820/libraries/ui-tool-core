import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {RequirementSentinelWithChildModulesComponent} from './requirement-sentinel-with-child-modules.component';

const routes: Routes = [
  {
    path: '',
    component: RequirementSentinelWithChildModulesComponent,
    children: [
      {
        path: '',
        loadChildren: () => import('./requirement-sentinel-child-module-1/requirement-sentinel-child-module-1.module')
          .then(m => m.RequirementSentinelChildModule1Module)
      }
    ]
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class RequirementSentinelWithChildModulesRoutingModule {

}
