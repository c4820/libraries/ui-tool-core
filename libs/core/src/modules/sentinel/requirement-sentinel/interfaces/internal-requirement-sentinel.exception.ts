import {IRequirementSentinelException} from './requirement-sentinel-exception.interface';
import {RequirementSentinelExceptionTypes} from '../constants/requirement-sentinel-exception-types';

export class InternalRequirementSentinelException<T> implements IRequirementSentinelException<T> {

  //#region Properties

  public readonly type: RequirementSentinelExceptionTypes;

  public readonly code: string;

  public readonly requirementName: string;

  public metadata?: T;

  //#endregion

  //#region Constructor

  public constructor(requirementName: string, code: string, metadata?: T) {
    this.requirementName = requirementName;
    this.type = RequirementSentinelExceptionTypes.INTERNAL;
    this.code = code;
    this.metadata = metadata;
  }


  //#endregion

}
