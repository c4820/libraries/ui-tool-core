import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        loadChildren: () => import('./multiple-validation-summarizer-basic-usage/multiple-validation-summarizer-basic-usage.module')
          .then(m => m.MultipleValidationSummarizerBasicUsageModule)
      }
    ])
  ]
})
export class MultipleValidationSummarizerDemoRoutingModule {

}
