export enum StackItemEvents {
  ITEM__DELETED = 'ITEM__DELETED',
  ITEM__PUSHED = 'ITEM__PUSHED'
}
