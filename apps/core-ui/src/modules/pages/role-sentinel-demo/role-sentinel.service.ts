import {Inject, Injectable} from '@angular/core';
import {IRoleSentinelService} from '@ui-tool/core';
import {Observable, of, Subject} from 'rxjs';
import {RoleService} from './role.service';
import {map} from 'rxjs/operators';

@Injectable()
export class RoleSentinelService implements IRoleSentinelService {

  //#region Properties

  // Validation event emitter.
  private readonly _validationEvent: Subject<void>;

  //#endregion

  //#region Constructor

  public constructor(protected readonly _roleService: RoleService) {
    this._validationEvent = new Subject<void>();
  }

  //#endregion

  //#region Methods

  public doValidation(): void {
    this._validationEvent.next(void (0));
  }

  public hookValidationEventAsync(): Observable<void> {
    return this._validationEvent.asObservable();
  }

  public hasAnyRoleAsync(names: string[]): Observable<boolean> {

    if (!names || !names.length) {
      return of(true);
    }

    return of(this._roleService.getRoles())
      .pipe(
        map(roles => {
          const itemIndex = roles.findIndex(role => names.indexOf(role) !== -1);
          return itemIndex !== -1;
        })
      );
  }

  //#endregion

}
