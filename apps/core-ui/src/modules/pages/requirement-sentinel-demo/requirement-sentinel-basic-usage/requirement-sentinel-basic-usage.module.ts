import {NgModule} from '@angular/core';
import {RequirementSentinelBasicUsageRoutingModule} from './requirement-sentinel-basic-usage-routing.module';
import {REQUIREMENT_HANDLER, RequirementSentinelModule} from '@ui-tool/core';
import {ReactiveFormsModule} from '@angular/forms';
import {
  BasicUsage1RequirementHandler
} from '@ui-demo/core/modules/pages/requirement-sentinel-demo/requirement-sentinel-basic-usage/basic-usage-1.requirement-handler';
import {
  BasicUsage2RequirementHandler
} from '@ui-demo/core/modules/pages/requirement-sentinel-demo/requirement-sentinel-basic-usage/basic-usage-2.requirement-handler';
import {
  RequirementSentinelBasicUsageComponent
} from '@ui-demo/core/modules/pages/requirement-sentinel-demo/requirement-sentinel-basic-usage/requirement-sentinel-basic-usage.component';
import {CommonModule} from '@angular/common';

@NgModule({
  imports: [
    RequirementSentinelModule.forRoot(),

    RequirementSentinelBasicUsageRoutingModule,
    ReactiveFormsModule,
    CommonModule
  ],
  declarations: [
    RequirementSentinelBasicUsageComponent
  ],
  providers: [
    {
      provide: REQUIREMENT_HANDLER,
      useClass: BasicUsage1RequirementHandler,
      multi: true
    },
    {
      provide: REQUIREMENT_HANDLER,
      useClass: BasicUsage2RequirementHandler,
      multi: true
    }
  ]
})
export class RequirementSentinelBasicUsageModule {

}
