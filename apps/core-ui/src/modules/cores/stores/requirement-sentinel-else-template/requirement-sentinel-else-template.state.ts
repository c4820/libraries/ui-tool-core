export interface IRequirementSentinelElseTemplateState {
  //#region Properties

  requirementSatisfied: boolean;

  //#endregion
}
