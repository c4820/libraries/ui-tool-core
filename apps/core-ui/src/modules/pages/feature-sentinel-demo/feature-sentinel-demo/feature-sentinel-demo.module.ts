import {NgModule} from '@angular/core';
import {FeatureSentinelDemoComponent} from './feature-sentinel-demo.component';
import {FeatureSentinelDemoRoutingModule} from './feature-sentinel-demo-routing.module';
import {FeatureSentinelModule} from '@ui-tool/core';
import {AuthenticationService} from '../authentication.service';
import {FeatureSentinelService} from '../feature-sentinel.service';
import {FEATURE_SENTINEL_SERVICE} from '@ui-tool/core';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    FeatureSentinelModule.forRoot({
      providers: [
        {
          provide: FEATURE_SENTINEL_SERVICE,
          useClass: FeatureSentinelService
        }
      ]
    }),

    FeatureSentinelDemoRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [
    FeatureSentinelDemoComponent
  ],
  exports: [
    FeatureSentinelDemoComponent
  ],
  providers: [
    AuthenticationService
  ]
})
export class FeatureSentinelDemoModule {

}
