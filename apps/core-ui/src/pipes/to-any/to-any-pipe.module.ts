import {NgModule} from '@angular/core';
import {ToAnyPipe} from './to-any.pipe';

@NgModule({
  declarations: [
    ToAnyPipe
  ],
  exports: [
    ToAnyPipe
  ]
})
export class ToAnyPipeModule {

}
