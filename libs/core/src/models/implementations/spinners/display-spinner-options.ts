import {Type} from '@angular/core';

export class DisplaySpinnerOptions {

  //#region Properties

  // Whether previous displayed spinner must be destroyed or not.
  purge?: boolean;

  // If no instance type is defined, the default spinner will be used instead.
  instanceType?: Type<any>;

  public closing?: () => boolean;

  public closed?: (force: boolean) => void;

  public invokedMethod?: (methodName: string, payload: any) => void;

  public closeText?: string;

  //#endregion

}
