import {NgModule} from '@angular/core';
import {RequirementSentinelChildModule1Component} from './requirement-sentinel-child-module-1.component';
import {RequirementSentinelChildModule1RoutingModule} from './requirement-sentinel-child-module-1-routing.module';
import {REQUIREMENT_HANDLER, REQUIREMENT_SENTINEL_EXCEPTION_PROCESSOR, RequirementSentinelModule} from '@ui-tool/core';
import {ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {
  RequirementSentinelWithChildModulesChildRequirementHandler
} from '@ui-demo/core/modules/pages/requirement-sentinel-demo/requirement-sentinel-with-child-modules/requirement-sentinel-child-module-1/requirement-sentinel-with-child-modules-child.requirement-handler';

@NgModule({
  imports: [
    RequirementSentinelModule.forRoot(),
    RequirementSentinelChildModule1RoutingModule,
    ReactiveFormsModule,
    CommonModule
  ],
  declarations: [
    RequirementSentinelChildModule1Component
  ],
  providers: [
    {
      provide: REQUIREMENT_HANDLER,
      useClass: RequirementSentinelWithChildModulesChildRequirementHandler,
      multi: true
    }
  ]
})
export class RequirementSentinelChildModule1Module {

}
