import {ChangeDetectionStrategy, ChangeDetectorRef, Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {combineLatest, Subscription} from 'rxjs';
import {
  RequirementSentinelWithLogStore
} from '@ui-demo/core/modules/cores/stores/requirement-sentinel-with-log/requirement-sentinel-with-log.store';
import {
  RequirementSentinelWithLogNames
} from '@ui-demo/core/modules/pages/requirement-sentinel-demo/requirement-sentinel-with-log/requirement-sentinel-with-log-names';
import {
  IMeetRequirementService,
  IRequirementSentinelException,
  REQUIREMENT_SENTINEL_SERVICE,
  RequirementSentinelExceptionTypes
} from '@ui-tool/core';
import {
  RequirementSentinelWithLogQuery
} from '@ui-demo/core/modules/cores/stores/requirement-sentinel-with-log/requirement-sentinel-with-log.query';
import {
  WithContextRequirementNames
} from '@ui-demo/core/modules/pages/requirement-sentinel-demo/requirement-sentinel-with-context/with-context-requirement-names';

@Component({
  selector: 'requirement-sentinel-with-context',
  templateUrl: 'requirement-sentinel-with-context.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequirementSentinelWithContextComponent implements OnInit, OnDestroy {

  //#region Properties

  private __userId: string | undefined;

  public readonly userIds = ['UNMET_REQUIREMENT', 'MET_REQUIREMENT'];

  public readonly RequirementNames = WithContextRequirementNames;

  //#endregion

  //#region Accessors

  public set userId(value: string | undefined) {
    this.__userId = value;
  }

  public get userId(): string | undefined {
    return this.__userId;
  }

  //#endregion

  //#region Constructor

  public constructor() {
  }

  //#endregion

  //#region Life cycle hook

  public ngOnInit(): void {
  }

  public ngOnDestroy(): void {
  }

  //#endregion
}
