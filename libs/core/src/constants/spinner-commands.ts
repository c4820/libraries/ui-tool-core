export class SpinnerCommands {

  //#region Properties

  public static readonly display = 'DISPLAY_SPINNER';

  public static readonly close = 'CLOSE_SPINNER';

  public static readonly purge = 'PURGE_SPINNER';

  //#endregion

}
