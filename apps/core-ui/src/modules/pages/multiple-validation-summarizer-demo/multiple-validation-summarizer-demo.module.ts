import {NgModule} from '@angular/core';
import {
  MultipleValidationSummarizerBasicUsageModule
} from '@ui-demo/core/modules/pages/multiple-validation-summarizer-demo/multiple-validation-summarizer-basic-usage/multiple-validation-summarizer-basic-usage.module';

@NgModule({
  imports: [
    MultipleValidationSummarizerBasicUsageModule
  ]
})
export class MultipleValidationSummarizerDemoModule {

}
