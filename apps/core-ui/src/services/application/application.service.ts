import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {AppSettings} from '@ui-demo/core/models/app-settings';
import {Observable} from 'rxjs';
import {PackageMetadata} from '@ui-demo/core/models/package-metadata';

@Injectable()
export class ApplicationService {

  //#region Properties

  private __settings: AppSettings | undefined;

  //#endregion

  //#region Constructor

  public constructor(private readonly __httpClient: HttpClient) {
  }

  //#endregion

  //#region Methods

  public getMetaDataAsync(): Observable<PackageMetadata> {
    return this.__httpClient.get<PackageMetadata>('assets/package.json');
  }

  //#endregion

}
