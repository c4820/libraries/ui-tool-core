import {ChangeDetectionStrategy, Component, Inject, OnInit} from '@angular/core';
import {FormArray, FormControl, FormGroup, Validators} from '@angular/forms';
import {VALIDATION_SUMMARIZER_SERVICE, IValidationSummarizerService, WINDOW} from '@ui-tool/core';
@Component({
  selector: 'validation-summarizer-class-demo-page',
  templateUrl: 'validation-summarizer-with-form-array-demo-page.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ValidationSummarizerWithFormArrayDemoPageComponent implements OnInit {

  //#region Properties

  private readonly __formArray: FormArray;

  private readonly __formGroup: FormGroup;

  //#endregion

  //#region Accessors

  public get formGroup(): FormGroup {
    return this.__formGroup;
  }

  public get formArray(): FormArray {
    return this.__formArray;
  }

  //#endregion

  //#region Constructor

  public constructor(@Inject(VALIDATION_SUMMARIZER_SERVICE)
                     protected readonly _validationSummarizerService: IValidationSummarizerService,
                     @Inject(WINDOW)
                     protected readonly _window: Window) {
    this.__formArray = new FormArray([]);
    this.__formGroup = new FormGroup({
      keyValuePairs: this.__formArray
    });
  }

  //#endregion

  //#region Life cycle hooks

  public ngOnInit(): void {
    for (let i = 0; i < 2; i++) {
      this.__formArray.push(new FormGroup({
        key: new FormControl('', [Validators.required]),
        value: new FormControl('',[Validators.required])
      }));
    }
  }

  //#endregion

  //#region Methods

  public doValidation(): void {
    this._validationSummarizerService.doFormControlsValidation(this.formGroup);
    if (this.formGroup.invalid) {
      this._window.alert('Invalid form');
      return;
    }
    this._window.alert('Valid');
  }

  //#endregion
}
