import {IStackService} from '../../interfaces/stack-service.interface';
import {Injectable} from '@angular/core';
import {Observable, ReplaySubject, Subject} from 'rxjs';
import {StackItemDeletedEvent} from '../../../models/implementations/stacks/stack-item-deleted-event';
import {StackItemPushedEvent} from '../../../models/implementations/stacks/stack-item-pushed-event';
import {StackItemOptions} from '../../../models/implementations/stacks/stack-item-options';
import {IStackItemEvent} from '../../../models/interfaces/stacks/stack-item-event.interface';
import {v4} from 'uuid';
import {IStackContext} from '../../../models/interfaces/stacks/stack-context.interface';

@Injectable()
export class StackService implements IStackService {

  //#region Properties

  // Mapping between container id & visibility emitter.
  private readonly __stackCodeToContext: {
    [stackCode: string]
      : IStackContext
  };

  //#endregion

  //#region Constructor

  public constructor() {
    this.__stackCodeToContext = {};
  }

  //#endregion

  //#region Methods

  public delete(stackCode: string, requestId: string): void {
    const context = this.__getContextByStackCode(stackCode);
    const itemContextIndex = context.items.findIndex(item => item.id === requestId);
    if (itemContextIndex < 0) {
      return;
    }

    const itemContext = context.items[itemContextIndex];
    if (!itemContext) {
      return;
    }

    if (itemContext.option?.onItemBeingRemoved?.() === false) {
      return;
    }

    context.items.splice(itemContextIndex, 1);
    const stackItemDeletedEvent = new StackItemDeletedEvent(stackCode, context.items.map(x => x.id));
    context.event$.next(stackItemDeletedEvent);
  }

  public hookStackEvent(stackCode: string): Observable<IStackItemEvent> {
    const context = this.__getContextByStackCode(stackCode);
    return context.event$.asObservable();
  }

  public purge(designatedStackCode?: string): void {
    if (!designatedStackCode) {

      const stackCodes = Object.keys(this.__stackCodeToContext);
      for (const stackCode of stackCodes) {
        this.purgeByStackCode(stackCode);
      }
      return;
    }

    this.purgeByStackCode(designatedStackCode);
  }

  public purgeByStackCode(stackCode: string): void {
    const context = this.__getContextByStackCode(stackCode);
    context.items.splice(0);

    const stackItemDeletedEvent = new StackItemDeletedEvent(stackCode, context.items.map(x => x.id));
    context.event$.next(stackItemDeletedEvent);
  }

  public push(stackCode: string, options?: StackItemOptions): string {
    // Initialize request id.
    const requestId = v4();

    const context = this.__getContextByStackCode(stackCode);
    if (options?.purge) {
      context.items.splice(0);
    }
    context.items.push({
      id: requestId,
      option: options
    });

    const itemIds = context.items.map(x => x.id);
    const stackItemPushedEvent = new StackItemPushedEvent(stackCode, requestId, itemIds);
    context.event$.next(stackItemPushedEvent);
    return requestId;
  }

  //#endregion

  //#region Internal methods

  private __getContextByStackCode(stackCode: string): IStackContext {
    if (!this.__stackCodeToContext[stackCode]) {
      this.__stackCodeToContext[stackCode] = {
        event$: new ReplaySubject<IStackItemEvent>(1),
        items: [],
      };
    }

    return this.__stackCodeToContext[stackCode];
  }

  //#endregion
}
