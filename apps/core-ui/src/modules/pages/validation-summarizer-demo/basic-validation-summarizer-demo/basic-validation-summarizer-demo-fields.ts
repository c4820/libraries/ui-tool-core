import {FormControl, FormGroup, Validators} from '@angular/forms';

export class BasicValidationSummarizerDemoFields {

  //#region Static properties

  public static readonly password = 'PASSWORD';

  //#endregion

  //#region Properties

  // tslint:disable-next-line:variable-name
  private readonly _formGroup: FormGroup;

  //#endregion

  //#region Constructor

  public constructor() {
    this._formGroup = new FormGroup({});

    const passwordControl = new FormControl('', [
      Validators.required,
      Validators.maxLength(10),
      Validators.minLength(3)
    ]);
    this._formGroup.addControl(BasicValidationSummarizerDemoFields.password, passwordControl);
  }

  //#endregion

  //#region Methods

  public toFormGroup(): FormGroup {
    return this._formGroup;
  }

  //#endregion

}
