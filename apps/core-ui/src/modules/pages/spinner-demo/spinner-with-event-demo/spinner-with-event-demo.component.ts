import {ChangeDetectionStrategy, Component, Inject} from '@angular/core';
import {SPINNER_SERVICE, ISpinnerService} from '@ui-tool/core';
import {ToastrService} from 'ngx-toastr';

@Component({
  selector: 'spinner-with-method-invoke-demo-event-demo',
  templateUrl: 'spinner-with-event-demo.component.html',
  styleUrls: ['./spinner-with-event-demo.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SpinnerWithEventDemoComponent {

  //#region Properties

  private readonly __spinnerRequestIds: string[];

  public readonly demoSpinnerId = 'DEMO_SPINNER_ID';

  public notAbleToCloseSpinner = false;

  //#endregion

  //#region Accessors

  public get ableToDisplaySpinner(): boolean {
    return this.__spinnerRequestIds.length > 0;
  }

  //#endregion

  //#region Constructor

  public constructor(@Inject(SPINNER_SERVICE)
                     protected readonly _spinnerService: ISpinnerService,
                     protected readonly _toastService: ToastrService) {
    this.__spinnerRequestIds = [];
  }

  //#endregion

  //#region Methods

  public displaySpinner(): void {
    const spinnerRequestId = this._spinnerService.displaySpinner(this.demoSpinnerId, {
      closed: () => {

        const index = this.__spinnerRequestIds.findIndex(x => x === spinnerRequestId);
        if (index < 0) {
          return;
        }

        this.__spinnerRequestIds.splice(index, 1);
        this._toastService.success('Spinner closed !');
      },
      closing: (): boolean => {
        if (!this.notAbleToCloseSpinner) {
          return true;
        }
        this._toastService.warning('Spinner cannot be closed');
        return false;
      }
    });
    this.__spinnerRequestIds.push(spinnerRequestId);
  }

  public deleteSpinner(): void {

    const length = this.__spinnerRequestIds.length;
    if (length < 1) {
      return;
    }

    const spinnerRequestId = this.__spinnerRequestIds[length - 1];
    this._spinnerService.deleteSpinner(this.demoSpinnerId, spinnerRequestId);
  }

  //#endregion
}
