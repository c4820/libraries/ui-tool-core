import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';
import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {ToastrModule} from 'ngx-toastr';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {MainLayoutModule} from '@ui-demo/core/modules/cores/main-layout/main-layout.module';
import {SMART_NAVIGATOR_SCREEN_CODE_RESOLVER, SmartNavigatorModule} from '@ui-tool/core';
import {ScreenCodeResolver} from '@ui-demo/core/modules/resolvers/screen-code.resolver';
import { AkitaNgDevtools } from '@datorama/akita-ngdevtools';
import { AkitaNgRouterStoreModule } from '@datorama/akita-ng-router-store';
import { Environment } from '../environments/environment';
import {HttpClientModule} from '@angular/common/http';
import {ApplicationService} from '@ui-demo/core/services/application/application.service';
import {AppGuard} from '@ui-demo/core/guards/app.guard';

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    HttpClientModule,
    MainLayoutModule,
    BrowserAnimationsModule,
    ToastrModule.forRoot(),
    BrowserModule,
    SmartNavigatorModule.forRoot(),
    AppRoutingModule,
    Environment.production ? [] : AkitaNgDevtools.forRoot(),
    AkitaNgRouterStoreModule
  ],
  providers: [
    ApplicationService,
    {
      provide: SMART_NAVIGATOR_SCREEN_CODE_RESOLVER,
      useClass: ScreenCodeResolver,
      multi: true,
    },
    AppGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}
