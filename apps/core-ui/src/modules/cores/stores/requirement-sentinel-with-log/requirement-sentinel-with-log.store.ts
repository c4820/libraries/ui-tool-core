import {Store, StoreConfig} from '@datorama/akita';
import {Injectable} from '@angular/core';
import {
  IRequirementSentinelWithLogState
} from '@ui-demo/core/modules/cores/stores/requirement-sentinel-with-log/requirement-sentinel-with-log.state';
import {IRequirementSentinelException} from '@ui-tool/core';

@Injectable({
  providedIn: 'root'
})
@StoreConfig({name: 'requirement-sentinel__with-log'})
export class RequirementSentinelWithLogStore extends Store<IRequirementSentinelWithLogState> {

  //#region Constructor

  public constructor() {
    super({
      externalExceptions: [],
      internalExceptions: []
    });
  }

  //#endregion

  //#region Methods

  public addExternalException<T>(instance: IRequirementSentinelException<T>): void {
    this.update(state => ({
      ...state,
      externalExceptions: state.externalExceptions.concat([instance])
    }));
  }

  public addInternalException<T>(instance: IRequirementSentinelException<T>): void {
    this.update(state => ({
      ...state,
      internalExceptions: state.internalExceptions.concat([instance])
    }));
  }

  //#endregion
}
