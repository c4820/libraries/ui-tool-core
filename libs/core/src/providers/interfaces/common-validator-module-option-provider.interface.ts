import {ICommonValidatorModuleOptions} from '../../models';

export interface ICommonValidatorModuleOptionProvider {

  //#region Methods

  // Get validation summarizer module option.
  getOption(): ICommonValidatorModuleOptions;

  //#endregion

}
