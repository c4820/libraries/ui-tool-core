import {RouterModule, Routes} from '@angular/router';
import {SpinnerWithEventDemoComponent} from './spinner-with-event-demo.component';
import {NgModule} from '@angular/core';

const routes: Routes = [
  {
    path: '',
    component: SpinnerWithEventDemoComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class SpinnerWithEventDemoRoutingModule {
}
