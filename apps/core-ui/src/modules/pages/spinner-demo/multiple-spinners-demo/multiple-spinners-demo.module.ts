import {NgModule} from '@angular/core';
import {MultipleSpinnersDemoComponent} from './multiple-spinners-demo.component';
import {MultipleSpinnersDemoRoutingModule} from './multiple-spinners-demo-routing.module';
import {SpinnerContainerModule} from '@ui-tool/core';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';

@NgModule({
  imports: [
    SpinnerContainerModule,
    MultipleSpinnersDemoRoutingModule,
    FormsModule,
    CommonModule
  ],
  declarations: [
    MultipleSpinnersDemoComponent
  ],
  exports: [
    MultipleSpinnersDemoComponent
  ]
})
export class MultipleSpinnersDemoModule {

}
