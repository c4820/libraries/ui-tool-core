import {Directive, Inject, Input, OnDestroy, OnInit, TemplateRef, ViewContainerRef} from '@angular/core';
import {FEATURE_SENTINEL_SERVICE} from '../../../constants/injectors/injectors';
import {IFeatureSentinelService} from './feature-sentinel-service.interface';
import {of, ReplaySubject, Subject, Subscription} from 'rxjs';
import {catchError, debounceTime, distinctUntilChanged, switchMap, tap} from 'rxjs/operators';

@Directive({
  selector: '[hasFeatureAccess]'
})
export class FeatureSentinelDirective implements OnInit, OnDestroy {

  //#region Properties

  // Name of feature.
  private __names: string[];

  private __elseTemplateRef?: TemplateRef<any>;

  // Raise event about displaying feature content.
  private __displayFeatureContent$: Subject<void>;

  // Subscription watch list.
  protected readonly _subscription: Subscription;

  //#endregion

  //#region Accessors

  @Input('hasFeatureAccess')
  public set name(value: string | string[]) {

    if (value instanceof Array) {
      this.__names = value as string[];
    } else {
      this.__names = [value];
    }

    this.__displayFeatureContent$.next();
  }

  @Input('hasFeatureAccessElse')
  public set elseTemplate(value: TemplateRef<any>) {
    this.__elseTemplateRef = value;
    this.__displayFeatureContent$.next();
  }

  //#endregion

  //#region Constructor

  public constructor(@Inject(FEATURE_SENTINEL_SERVICE)
                     protected readonly _requireFeaturePermissionService: IFeatureSentinelService,
                     protected readonly _viewContainerRef: ViewContainerRef,
                     protected readonly _templateRef: TemplateRef<any>) {

    this.__names = [];
    this.__displayFeatureContent$ = new ReplaySubject<void>();
    this._subscription = new Subscription();
  }

//#endregion

  //#region Methods

  public ngOnInit(): void {

    // Display feature subscription.
    const displayFeatureContentSubscription = this.__displayFeatureContent$
      .pipe(
        switchMap(() => {
          return this._requireFeaturePermissionService
            .ableToAccessFeaturesAsync(this.__names)
            .pipe(
              catchError(_ => of(false))
            );
        }),
        distinctUntilChanged()
      )
      .subscribe(ableToAccessFeature => {
        this._viewContainerRef.clear();
        if (!ableToAccessFeature) {
          if (this.__elseTemplateRef) {
            const elseView = this._viewContainerRef.createEmbeddedView(this.__elseTemplateRef);
            elseView.markForCheck();
          }
          return;
        } else {
          const embeddedView = this._viewContainerRef.createEmbeddedView(this._templateRef);
          embeddedView.markForCheck();
        }
      });
    this._subscription.add(displayFeatureContentSubscription);

    // Hook validation event from service.
    const hookValidationEventSubscription = this._requireFeaturePermissionService
      .hookValidationEventAsync()
      .subscribe(() => {
        this.__displayFeatureContent$.next();
      });
    this._subscription.add(hookValidationEventSubscription);

    this.__displayFeatureContent$.next();
  }

  public ngOnDestroy(): void {
    this.__displayFeatureContent$.complete();
    this._subscription?.unsubscribe();
  }

  //#endregion
}
