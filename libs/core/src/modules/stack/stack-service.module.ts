import {ModuleWithProviders, NgModule} from '@angular/core';
import {STACK_SERVICE} from '../../constants';
import {StackService} from '../../services/implementations/stacks/stack.service';

@NgModule({})
export class StackServiceModule {

  //#region Static methods

  public static forRoot(): ModuleWithProviders<StackServiceModule> {
    return {
      ngModule: StackServiceModule,
      providers: [
        {
          provide: STACK_SERVICE,
          useClass: StackService
        }
      ]
    };
  }

  //#endregion

}
