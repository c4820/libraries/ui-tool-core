import {ChangeDetectionStrategy, Component, Inject} from '@angular/core';
import {DOCUMENT} from '@angular/common';

@Component({
  selector: 'navigation-bar',
  templateUrl: 'navigation-bar.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavigationBarComponent {

  //#region Constructor

  public constructor(@Inject(DOCUMENT) private readonly __document: Document) {
  }

  //#endregion

  //#region Methods

  public toggleSidebar(): void {
    this.__document.body.classList.toggle('toggle-sidebar');
  }

  //#endregion
}
