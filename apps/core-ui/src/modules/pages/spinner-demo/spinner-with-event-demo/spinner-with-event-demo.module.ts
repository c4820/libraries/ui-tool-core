import {NgModule} from '@angular/core';
import {SpinnerWithEventDemoComponent} from './spinner-with-event-demo.component';
import {SpinnerWithEventDemoRoutingModule} from './spinner-with-event-demo-routing.module';
import {SpinnerContainerModule} from '@ui-tool/core';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';

@NgModule({
  imports: [
    SpinnerContainerModule,
    SpinnerWithEventDemoRoutingModule,
    FormsModule,
    CommonModule
  ],
  declarations: [
    SpinnerWithEventDemoComponent
  ],
  exports: [
    SpinnerWithEventDemoComponent
  ]
})
export class SpinnerWithEventDemoModule {

}
