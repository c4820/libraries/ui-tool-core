import {IRequirementHandler} from '@ui-tool/core';
import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {
  WithContextRequirementNames
} from '@ui-demo/core/modules/pages/requirement-sentinel-demo/requirement-sentinel-with-context/with-context-requirement-names';
import {
  IWithContextUserAvailabilityRequirementContext
} from '@ui-demo/core/modules/pages/requirement-sentinel-demo/requirement-sentinel-with-context/with-context-user-availability-requirement-context.interface';

@Injectable()
export class WithContextUserAvailabilityRequirementHandler implements IRequirementHandler {


  //#region Properties

  public readonly name: string;

  //#endregion

  //#region Constructor

  public constructor() {
    this.name = WithContextRequirementNames.USER_AVAILABILITY;
  }


  //#endregion

  //#region Methods

  public shouldRequirementMetAsync(context: IWithContextUserAvailabilityRequirementContext)
    : Observable<boolean> {
    console.log(`Context is executed: `);
    console.log(context);
    switch (context.userId) {
      case 'MET_REQUIREMENT':
        return of(true);

      default:
        return of(false);
    }
  }

  //#endregion
}
