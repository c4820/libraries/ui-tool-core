import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {ValidationSummarizerClassDemoPageComponent} from './validation-summarizer-class-demo-page.component';

const routes: Routes = [
  {
    path: '',
    component: ValidationSummarizerClassDemoPageComponent
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ]
})
export class ValidationSummarizerClassDemoPageRoutingModule {

}
