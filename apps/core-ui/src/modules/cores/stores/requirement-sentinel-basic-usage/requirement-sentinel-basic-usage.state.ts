export interface IRequirementSentinelBasicUsageState {
  //#region Properties

  usageOneEnabled: boolean;
  usageTwoEnabled: boolean;

  //#endregion
}
