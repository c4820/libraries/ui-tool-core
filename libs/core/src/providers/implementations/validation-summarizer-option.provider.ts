import {IValidationSummarizerOptionProvider} from '../interfaces/validation-summarizer-options-provider.interface';
import {IValidationSummarizerModuleOptions} from '../../models/interfaces/validation-summarizers/validation-summarizer-module-options.interface';
import {Inject} from '@angular/core';
import {VALIDATION_SUMMARIZER_OPTIONS} from '../../constants/injectors/internal-injectors';
import {merge as lodashMerge} from 'lodash-es';

export class ValidationSummarizerOptionProvider
  implements IValidationSummarizerOptionProvider {

  //#region Constructor

  public constructor(@Inject(VALIDATION_SUMMARIZER_OPTIONS)
                     protected options: IValidationSummarizerModuleOptions[]) {
  }

  //#endregion

  //#region Methods

  public getOption(): IValidationSummarizerModuleOptions {
    let finalOption = {};
    for (const option of this.options) {
      finalOption = lodashMerge(finalOption, option);
    }

    return finalOption;
  }

  //#endregion
}
