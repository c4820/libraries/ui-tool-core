export enum RequirementSentinelExceptionTypes {
  INTERNAL = 'INTERNAL',
  EXTERNAL = 'EXTERNAL'
}
