/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [
    "./apps/core-ui/src/**/*.{html,js}",
    "./libs/core/src/**/*.{html,js}"
  ],
  theme: {
    extend: {},
  },
  plugins: [],
  corePlugins: {
    visibility: false
  }
}
