const path = require('path');
const process = require('process');
const yargs = require('yargs/yargs');
const {hideBin} = require('yargs/helpers');
const dayjs = require('dayjs');
const fs = require('fs');
const {unlinkSync} = require("fs");
const {execSync} = require("child_process");

const execute = () => {

  // Get the environment variable.
  const { branch, dryRun, dist, buildNumber, previewApiKey, releaseApiKey } = yargs(hideBin(process.argv)).argv;

  const utcNow = new Date();
  const szDate = dayjs(utcNow).format('YYYYMMDD');

  const actualOutputFolder = path.join(process.cwd(), dist);
  if (!fs.existsSync(actualOutputFolder)) {
    throw new Error(`Library output folder is not found: ${actualOutputFolder}`);
  }

  const packageJsonFile = path.join(actualOutputFolder, 'package.json');
  if (!fs.existsSync(packageJsonFile)) {
    throw new Error(`Package file is not found: ${packageJsonFile}`);
  }

  const packageContent = fs.readFileSync(packageJsonFile);
  const jsonContent = JSON.parse(packageContent);

  // Whether package is released or not.
  let isReleased = false;

  let version = '';
 if (branch.startsWith('release/')) {
    version = branch.replace('release/', '');
    isReleased = true;
  }
  else if (branch.startsWith('preview/')) {
    version = branch.replace('preview/', '');
    version = version + `-beta.${szDate}${buildNumber}`;
  } else {
    throw new Error(`Branch ${branch} does not support the deployment.`);
  }

  const npmCredentialFile = path.join(actualOutputFolder, '.npmrc');
  if (fs.existsSync(npmCredentialFile)) {
    unlinkSync(npmCredentialFile);
  }

  jsonContent['version'] = version;
  fs.writeFileSync(packageJsonFile, JSON.stringify(jsonContent), {
    encoding: 'utf8'
  });

  console.log(`Writing .npmrc file ...`);
  if (!isReleased) {
    fs.appendFileSync(npmCredentialFile, `@ui-tool:registry=https://www.myget.org/F/ui-tool/npm/\n`);
    fs.appendFileSync(npmCredentialFile, `//www.myget.org/F/ui-tool/npm/:always-auth=true\n`);
    fs.appendFileSync(npmCredentialFile, `//www.myget.org/F/ui-tool/npm/:_authToken=${previewApiKey}`);
  } else {
    fs.appendFileSync(npmCredentialFile, `ui-tool:registry=https://www.npmjs.org/F/ui-tool\n`);
    fs.appendFileSync(npmCredentialFile, `always-auth=true\n`);
    fs.appendFileSync(npmCredentialFile, `//registry.npmjs.org/:_authToken=${releaseApiKey}`);
  }

  execSync(`npm publish --dry-run=${dryRun}`, {
    cwd: actualOutputFolder
  })
};

execute();
