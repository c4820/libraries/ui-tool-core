import {Observable} from 'rxjs';
import {DisplaySpinnerOptions} from '../../models/implementations/spinners/display-spinner-options';
import {DisplaySpinnerCommand} from '../../models/implementations/spinners/display-spinner-command';
import {DeleteSpinnerCommand} from '../../models/implementations/spinners/delete-spinner-command';

export interface ISpinnerService {

  //#region Methods

  // Display spinner
  displaySpinner(containerId: string, options?: DisplaySpinnerOptions): string;

  // Delete the last spinner request.
  deleteSpinner(containerId: string, id: string): void;

  // Delete all spinners.
  deleteSpinners(containerId?: string): void;

  // Listen to visibility changed asynchronously.
  hookSpinnerVisibilityEvent(containerId: string): Observable<DisplaySpinnerCommand | DeleteSpinnerCommand>;

  //#endregion
}
