import {Subject} from 'rxjs';
import {IStackItemEvent} from './stack-item-event.interface';
import {IStackItemContext} from './stack-item-context.interface';

export interface IStackContext {

  //#region Properties

  event$: Subject<IStackItemEvent>;

  items: IStackItemContext[];

  //#endregion

}
