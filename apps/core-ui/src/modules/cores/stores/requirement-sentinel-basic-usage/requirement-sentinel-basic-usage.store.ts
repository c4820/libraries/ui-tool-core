import {Store, StoreConfig} from '@datorama/akita';
import {
  IRequirementSentinelBasicUsageState
} from '@ui-demo/core/modules/cores/stores/requirement-sentinel-basic-usage/requirement-sentinel-basic-usage.state';
import {Injectable} from '@angular/core';
import {
  RequirementSentinelBasicUsageNames
} from '@ui-demo/core/modules/pages/requirement-sentinel-demo/requirement-sentinel-basic-usage/requirement-sentinel-basic-usage-names';

@Injectable({
  providedIn: 'root'
})
@StoreConfig({name: 'requirement-sentinel__basic-usage'})
export class RequirementSentinelBasicUsageStore extends Store<IRequirementSentinelBasicUsageState> {

  //#region Constructor

  public constructor() {
    super({
      usageOneEnabled: false,
      usageTwoEnabled: false
    });
  }

  //#endregion

  //#region Methods

  public setRequirementSatisfaction(name: string, satisfied: boolean): void {
    switch (name) {
      case RequirementSentinelBasicUsageNames.BASIC_USAGE_1:
        this.update({
          usageOneEnabled: satisfied
        });
        break;

      case RequirementSentinelBasicUsageNames.BASIC_USAGE_2:
        this.update({
          usageTwoEnabled: satisfied
        });
        break;
    }
  }

  //#endregion
}
