export interface ISpinnerCommand {

  //#region Properties

  readonly kind: string;

  readonly containerId: string;

  //#endregion

}
