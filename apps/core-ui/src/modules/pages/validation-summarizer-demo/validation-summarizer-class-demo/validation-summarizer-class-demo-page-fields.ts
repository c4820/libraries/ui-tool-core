import {FormControl, FormGroup, Validators} from '@angular/forms';

export class ValidationSummarizerClassDemoPageFields {

  //#region Static properties

  public static readonly username = 'USERNAME';

  public static readonly password = 'PASSWORD';

  //#endregion

  //#region Properties

  private readonly __formGroup: FormGroup;

  //#endregion

  //#region Constructor

  public constructor() {
    this.__formGroup = new FormGroup({});

    const nameControl = new FormControl(null, [Validators.required]);
    const passwordControl = new FormControl('', [
      Validators.required,
      Validators.maxLength(10),
      Validators.minLength(3)
    ]);

    this.__formGroup.addControl(ValidationSummarizerClassDemoPageFields.username, nameControl);
    this.__formGroup.addControl(ValidationSummarizerClassDemoPageFields.password, passwordControl);
  }

  //#endregion

  //#region Methods

  public toFormGroup(): FormGroup {
    return this.__formGroup;
  }

  //#endregion

}
