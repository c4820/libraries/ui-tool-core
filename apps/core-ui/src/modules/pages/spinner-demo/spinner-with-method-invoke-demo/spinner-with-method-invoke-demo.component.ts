import {ChangeDetectionStrategy, Component, Inject} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {ISpinnerService, SPINNER_SERVICE} from '@ui-tool/core';
import {MethodInvokeSpinnerComponent} from '../custom-spinners/method-invoke-spinner/method-invoke-spinner.component';

@Component({
  selector: 'spinner-with-method-invoke-demo',
  templateUrl: 'spinner-with-method-invoke-demo.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SpinnerWithMethodInvokeDemoComponent {

  //#region Properties

  private readonly __spinnerRequestIds: string[];

  public readonly demoSpinnerId = 'DEMO_SPINNER_ID';

  public closeWhenInvoke = false;

  //#endregion

  //#region Accessors

  public get ableToDisplaySpinner(): boolean {
    return this.__spinnerRequestIds.length > 0;
  }

  //#endregion

  //#region Constructor

  public constructor(@Inject(SPINNER_SERVICE)
                     protected readonly _spinnerService: ISpinnerService,
                     protected readonly _toastService: ToastrService) {
    this.__spinnerRequestIds = [];
  }

  //#endregion

  //#region Methods

  public displaySpinner(): void {
    const spinnerRequestId = this._spinnerService.displaySpinner(this.demoSpinnerId, {
      instanceType: MethodInvokeSpinnerComponent,
      closed: () => {
        const index = this.__spinnerRequestIds.findIndex(x => x === spinnerRequestId);
        if (index < 0) {
          return;
        }

        this.__spinnerRequestIds.splice(index, 1);
      },
      invokedMethod: (methodName: string, data: object) => {
        if (!this.closeWhenInvoke) {
          this._toastService.error(`Method ${methodName} is called, but the checkbox is not checked.`);
          return;
        }
        this._toastService.success(`Method ${methodName} is called, the spinner will be closed.`);
        this._spinnerService.deleteSpinner(this.demoSpinnerId, spinnerRequestId);
      }
    });
    this.__spinnerRequestIds.push(spinnerRequestId);
  }

  public deleteSpinner(): void {

    const length = this.__spinnerRequestIds.length;
    if (length < 1) {
      return;
    }

    const spinnerRequestId = this.__spinnerRequestIds[length - 1];
    this._spinnerService.deleteSpinner(this.demoSpinnerId, spinnerRequestId);
  }

  //#endregion
}
