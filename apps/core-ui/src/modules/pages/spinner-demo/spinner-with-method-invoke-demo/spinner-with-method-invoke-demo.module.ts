import {NgModule} from '@angular/core';
import {SpinnerWithMethodInvokeDemoComponent} from './spinner-with-method-invoke-demo.component';
import {SpinnerContainerModule} from '@ui-tool/core';
import {FormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {SpinnerWithMethodInvokeDemoRoutingModule} from './spinner-with-method-invoke-demo-routing.module';

@NgModule({
  imports: [
    SpinnerWithMethodInvokeDemoRoutingModule,
    SpinnerContainerModule,
    FormsModule,
    CommonModule
  ],
  declarations: [
    SpinnerWithMethodInvokeDemoComponent
  ],
  exports: [
    SpinnerWithMethodInvokeDemoComponent
  ]
})
export class SpinnerWithMethodInvokeDemoModule {

}
