import {Component, HostBinding, Inject} from '@angular/core';
import {DISPLAY_SPINNER_OPTIONS, SPINNER_HOST, SPINNER_REQUEST_ID, SPINNER_SERVICE} from '../../../constants';
import {DisplaySpinnerOptions} from '../../../models';
import {ISpinnerService} from '../../../services';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'basic-spinner',
  templateUrl: 'basic-spinner.component.html',
  styleUrls: ['basic-spinner.component.scss']
})
export class BasicSpinnerComponent {

  //#region Accessors


  @HostBinding('attr.request-id')
  public get requestId(): string {
    return this._requestId;
  }

  //#endregion

  //#region Constructor

  public constructor(
    @Inject(SPINNER_REQUEST_ID)
    protected readonly _requestId: string,
    @Inject(DISPLAY_SPINNER_OPTIONS)
    public readonly options: DisplaySpinnerOptions,
    @Inject(SPINNER_HOST)
    public readonly host: string,
    @Inject(SPINNER_SERVICE)
    protected readonly _spinnerService: ISpinnerService) {
  }

  //#endregion

  //#region Methods

  public cancel(): void {
    this._spinnerService.deleteSpinner(this.host, this.requestId);
  }

  //#endregion
}
