import {IRequirementHandler} from '@ui-tool/core';
import {Injectable} from '@angular/core';
import {Observable, of} from 'rxjs';
import {
  RequirementSentinelWithChildModulesNames
} from '@ui-demo/core/modules/pages/requirement-sentinel-demo/requirement-sentinel-with-child-modules/requirement-sentinel-with-child-modules-names';

@Injectable()
export class RequirementSentinelWithChildModulesChildRequirementHandler implements IRequirementHandler {

  //#region Properties

  public readonly name: string;

  //#endregion

  //#region Constructor

  public constructor() {
    this.name = RequirementSentinelWithChildModulesNames.CHILD;
  }

  //#endregion

  //#region Methods

  public shouldRequirementMetAsync(): Observable<boolean> {
    return of(true);
  }

  //#endregion
}
