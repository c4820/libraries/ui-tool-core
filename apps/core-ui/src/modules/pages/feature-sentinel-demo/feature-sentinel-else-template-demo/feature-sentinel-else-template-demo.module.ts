import {NgModule} from '@angular/core';
import {FeatureSentinelElseTemplateDemoComponent} from './feature-sentinel-else-template-demo.component';
import {FeatureSentinelElseTemplateDemoRoutingModule} from './feature-sentinel-else-template-demo-routing.module';
import {FeatureSentinelModule} from '@ui-tool/core';
import {AuthenticationService} from '../authentication.service';
import {FeatureSentinelService} from '../feature-sentinel.service';
import {FEATURE_SENTINEL_SERVICE} from '@ui-tool/core';
import {ReactiveFormsModule} from '@angular/forms';

@NgModule({
  imports: [
    FeatureSentinelModule.forRoot({
      providers: [
        {
          provide: FEATURE_SENTINEL_SERVICE,
          useClass: FeatureSentinelService
        }
      ]
    }),

    FeatureSentinelElseTemplateDemoRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [
    FeatureSentinelElseTemplateDemoComponent
  ],
  exports: [
    FeatureSentinelElseTemplateDemoComponent
  ],
  providers: [
    AuthenticationService
  ]
})
export class FeatureSentinelElseTemplateDemoModule {

}
