import {StackItemOptions} from '../../implementations';

export interface IStackItemContext {

  //#region Properties

  id: string;

  option?: StackItemOptions;

  //#endregion

}
