import {NgModule} from '@angular/core';
import {RequirementSentinelWithContextComponent} from './requirement-sentinel-with-context.component';
import {RequirementSentinelWithContextRoutingModule} from './requirement-sentinel-with-context-routing.module';
import {REQUIREMENT_HANDLER, RequirementSentinelModule} from '@ui-tool/core';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {CommonModule} from '@angular/common';
import {
  WithContextUserAvailabilityRequirementHandler
} from '@ui-demo/core/modules/pages/requirement-sentinel-demo/requirement-sentinel-with-context/with-context-user-availability.requirement-handler';

@NgModule({
  imports: [
    RequirementSentinelModule.forRoot(),
    RequirementSentinelWithContextRoutingModule,
    ReactiveFormsModule,
    CommonModule,
    FormsModule
  ],
  declarations: [
    RequirementSentinelWithContextComponent
  ],
  providers: [
    {
      provide: REQUIREMENT_HANDLER,
      useClass: WithContextUserAvailabilityRequirementHandler,
      multi: true
    }
  ]
})
export class RequirementSentinelWithContextModule {

}
