import {NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {CommonValidatorModule, HasAnyValidatorsModule, ValidationSummarizerModule} from '@ui-tool/core';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'class-demo',
        loadChildren: () => import('./validation-summarizer-class-demo/validation-summarizer-class-demo-page.module')
          .then(m => m.ValidationSummarizerClassDemoPageModule)
      },
      {
        path: 'form-array-demo',
        loadChildren: () => import('./validation-summarizer-with-form-array-demo/validation-summarizer-with-form-array-demo-page.module')
          .then(m => m.ValidationSummarizerWithFormArrayDemoPageModule)
      },
      {
        path: 'template-driven',
        loadChildren: () => import('./template-driven-validation-summarizer/template-driven-validation-summarizer.module')
          .then(m => m.TemplateDrivenValidationSummarizerModule)
      },
      {
        path: '',
        loadChildren: () => import('./basic-validation-summarizer-demo/basic-validation-summarizer-demo.module')
          .then(m => m.BasicValidationSummarizerDemoModule)
      }
    ]),
    HasAnyValidatorsModule.forRoot(),
    CommonValidatorModule.forRoot(),
    ValidationSummarizerModule.forRoot({})
  ],
  exports: [
    RouterModule
  ]
})
export class ValidationSummarizerDemoModule {

}
