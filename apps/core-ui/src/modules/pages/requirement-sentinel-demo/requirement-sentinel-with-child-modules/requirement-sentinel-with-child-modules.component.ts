import {ChangeDetectionStrategy, Component} from '@angular/core';
import {
  RequirementSentinelWithChildModulesNames
} from '@ui-demo/core/modules/pages/requirement-sentinel-demo/requirement-sentinel-with-child-modules/requirement-sentinel-with-child-modules-names';

@Component({
  selector: 'requirement-sentinel-with-child-modules',
  templateUrl: 'requirement-sentinel-with-child-modules.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequirementSentinelWithChildModulesComponent {

  //#region Properties

  public readonly RequirementNames = RequirementSentinelWithChildModulesNames;

  //#endregion
}
