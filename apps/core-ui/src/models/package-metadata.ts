export class PackageMetadata {

  //#region Constructor

  public constructor(public readonly name: string,
                     public readonly version: string) {
  }

  //#endregion
}
