import {Query} from '@datorama/akita';
import {ApplicationStore} from '@ui-demo/core/modules/cores/stores/application.store';
import {Injectable} from '@angular/core';
import {IApplicationState} from '@ui-demo/core/modules/cores/stores/application.state';

@Injectable({
  providedIn: 'root'
})
export class ApplicationQuery extends Query<IApplicationState> {

  //#region Properties

  public readonly version$ = this.select('version');

  //#endregion


  //#region Constructor

  public constructor(protected readonly _store: ApplicationStore) {
    super(_store);
  }

  //#endregion
}
