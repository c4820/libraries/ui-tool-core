import {ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit} from '@angular/core';
import {combineLatest, Observable, of, Subscription} from 'rxjs';
import {
  RequirementSentinelBasicUsageNames
} from '@ui-demo/core/modules/pages/requirement-sentinel-demo/requirement-sentinel-basic-usage/requirement-sentinel-basic-usage-names';
import {
  RequirementSentinelBasicUsageQuery
} from '@ui-demo/core/modules/cores/stores/requirement-sentinel-basic-usage/requirement-sentinel-basic-usage.query';
import {map, tap} from 'rxjs/operators';
import {
  RequirementSentinelBasicUsageStore
} from '@ui-demo/core/modules/cores/stores/requirement-sentinel-basic-usage/requirement-sentinel-basic-usage.store';

@Component({
  selector: 'requirement-sentinel-demo',
  templateUrl: 'requirement-sentinel-basic-usage.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RequirementSentinelBasicUsageComponent implements OnInit, OnDestroy {

  //#region Properties

  public readonly RequirementNames = RequirementSentinelBasicUsageNames;

  private __nameToStatus: Record<string, boolean>;

  private readonly __subscriptions = new Subscription();

  //#endregion

  //#region Accessors

  public get nameToStatus(): Record<string, boolean> {
    return this.__nameToStatus;
  }

  //#endregion

  //#region Constructor

  public constructor(private readonly __query: RequirementSentinelBasicUsageQuery,
                     private readonly __store: RequirementSentinelBasicUsageStore,
                     private readonly __changeDetectorRef: ChangeDetectorRef) {
    this.__nameToStatus = {
      [RequirementSentinelBasicUsageNames.BASIC_USAGE_1]: false,
      [RequirementSentinelBasicUsageNames.BASIC_USAGE_2]: false
    };
  }

  //#endregion

  //#region Life cycle hook

  public ngOnInit(): void {
    const nameToStatus$: Record<string, Observable<boolean>> = {
      [RequirementSentinelBasicUsageNames.BASIC_USAGE_1]: this.__query.usageOneEnabled$,
      [RequirementSentinelBasicUsageNames.BASIC_USAGE_2]: this.__query.usageTwoEnabled$
    };

    const names = Object.keys(nameToStatus$);
    const getEnabledStatusObservables: Observable<{ name: string, enabled: boolean }>[] = [];
    for (const name of names) {
      const getEnabledStatusObservable = nameToStatus$[name]
        .pipe(
          map(enabled => ({name, enabled}))
        );
      getEnabledStatusObservables.push(getEnabledStatusObservable);
    }
    const getEnabledStatusSubscription = combineLatest(getEnabledStatusObservables)
      .subscribe(nameToStatusKeyValuePairs => {
        const nameToStatus: Record<string, boolean> = {};
        for (const nameToStatusKeyValuePair of nameToStatusKeyValuePairs) {
          nameToStatus[nameToStatusKeyValuePair.name] = nameToStatusKeyValuePair.enabled;
        }
        this.__nameToStatus = nameToStatus;
        this.__changeDetectorRef.markForCheck();
      });
    this.__subscriptions.add(getEnabledStatusSubscription);
  }

  public ngOnDestroy(): void {
    this.__subscriptions.unsubscribe();
  }

  //#endregion

  //#region Methods

  public dissatisfyRequirement(name: string): void {
    this.__store.setRequirementSatisfaction(name, false);
  }

  public satisfyRequirement(name: string): void {
    this.__store.setRequirementSatisfaction(name, true);

  }

  //#endregion
}
