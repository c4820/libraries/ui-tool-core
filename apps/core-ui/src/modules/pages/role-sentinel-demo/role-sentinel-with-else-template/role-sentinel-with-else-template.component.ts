import {ChangeDetectionStrategy, Component, Inject, OnDestroy, OnInit} from '@angular/core';
import {Subscription} from 'rxjs';
import {RoleSentinelWithElseTemplateFields} from './role-sentinel-with-else-template-fields';
import {FormGroup} from '@angular/forms';
import {RoleService} from '../role.service';
import {IRoleSentinelService, ROLE_SENTINEL_SERVICE} from '@ui-tool/core';
import { UserRoles } from '../user-roles';

@Component({
  selector: 'role-sentinel-with-else-template',
  templateUrl: 'role-sentinel-with-else-template.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class RoleSentinelWithElseTemplateComponent implements OnInit, OnDestroy {

  //#region Properties

  private readonly __fields = new RoleSentinelWithElseTemplateFields();

  private readonly __subscriptions = new Subscription();

  public readonly UserRoles = UserRoles;


  //#endregion

  //#region Accessors

  public get formGroup(): FormGroup {
    return this.__fields.formGroup;
  }

  //#endregion

  //#region Constructor

  public constructor(public readonly _roleService: RoleService,
                     @Inject(ROLE_SENTINEL_SERVICE)
                     protected readonly _roleSentinelService: IRoleSentinelService) {
  }

  //#endregion

  //#region Life cycle hook

  public ngOnInit(): void {
    const subscription = this.formGroup.valueChanges
      .subscribe(value => {
        const keys = Object.keys(value)
          .filter(key => value[key]);
        this._roleService.updateRoles(keys);
        this._roleSentinelService.doValidation();
      });
    this.__subscriptions.add(subscription);
  }

  public ngOnDestroy(): void {
    this.__subscriptions.unsubscribe();
  }

  //#endregion
}
