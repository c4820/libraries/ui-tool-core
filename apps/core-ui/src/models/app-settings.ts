export class AppSettings {

  //#region Constructor

  public constructor(public readonly apiUrl: string) {
  }

  //#endregion

}
