import {NgModule} from '@angular/core';
import {BasicValidationSummarizerDemoComponent} from './basic-validation-summarizer-demo.component';
import {ValidationSummarizerModule, CommonValidatorModule} from '@ui-tool/core';
import {AbstractControl, NgControl, ReactiveFormsModule} from '@angular/forms';
import {RouterModule} from '@angular/router';

@NgModule({
    imports: [
        RouterModule.forChild([
            {
                path: '',
                component: BasicValidationSummarizerDemoComponent
            }
        ]),
        ValidationSummarizerModule.forChild({}),
        ReactiveFormsModule,
        CommonValidatorModule.forChild({
          defaultValidationClasses: ['text-danger'],
          defaultControlValidationClasses: ['border', 'border-danger']
        })
    ],
  declarations: [
    BasicValidationSummarizerDemoComponent
  ],
  exports: [
    BasicValidationSummarizerDemoComponent
  ]
})
export class BasicValidationSummarizerDemoModule {

}
