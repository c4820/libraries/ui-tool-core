import {IRequirementSentinelException} from './requirement-sentinel-exception.interface';
import {RequirementSentinelExceptionTypes} from '../constants/requirement-sentinel-exception-types';

export class ExternalRequirementSentinelException<T> implements IRequirementSentinelException<T> {

  //#region Properties

  public readonly requirementName: string;

  public readonly code: string;

  public metadata?: T;

  readonly type: RequirementSentinelExceptionTypes;

  //#endregion

  //#region Constructor

  public constructor(requirementName: string, code: string, metadata?: T) {
    this.requirementName = requirementName;
    this.type = RequirementSentinelExceptionTypes.EXTERNAL;
    this.code = code;
    this.metadata = metadata;
  }

  //#endregion
}
