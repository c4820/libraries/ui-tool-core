import {Query} from '@datorama/akita';
import {Injectable} from '@angular/core';
import {
  IRequirementSentinelWithLogState
} from '@ui-demo/core/modules/cores/stores/requirement-sentinel-with-log/requirement-sentinel-with-log.state';
import {
  RequirementSentinelWithLogStore
} from '@ui-demo/core/modules/cores/stores/requirement-sentinel-with-log/requirement-sentinel-with-log.store';

@Injectable({
  providedIn: 'root'
})
export class RequirementSentinelWithLogQuery extends Query<IRequirementSentinelWithLogState> {

  //#region Properties

  public readonly internalExceptions$ = this.select(store => store.internalExceptions);
  public readonly externalExceptions$ = this.select(store => store.externalExceptions);

  //#endregion

  //#region Constructor

  public constructor(store: RequirementSentinelWithLogStore) {
    super(store);
  }

  //#endregion
}
