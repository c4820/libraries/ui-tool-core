import {Observable} from 'rxjs';
import {StackItemOptions} from '../../models/implementations/stacks/stack-item-options';
import {IStackItemEvent} from '../../models/interfaces/stacks/stack-item-event.interface';

export interface IStackService {
  //#region Methods

  // Create an item in stack
  push(stackCode: string, options?: StackItemOptions): string;

  // Delete an item from stack
  delete(stackCode: string, id: string): void;

  // Delete all spinners.
  purgeByStackCode(stackCode?: string): void;

  purge(stackCode: string): void;

  // Listen to visibility changed asynchronously.
  hookStackEvent(stackCode: string): Observable<IStackItemEvent>;

  //#endregion
}
