export class UserFeatures {

  //#region Properties

  public static readonly addUser = 'user.add';

  public static readonly editAnyUser = 'user.edit';

  public static readonly deleteAnyUser = 'user.delete';

  //#endregion

}
