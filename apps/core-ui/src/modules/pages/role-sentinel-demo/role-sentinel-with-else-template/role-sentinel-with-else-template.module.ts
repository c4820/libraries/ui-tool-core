import {NgModule} from '@angular/core';
import {RoleSentinelWithElseTemplateComponent} from './role-sentinel-with-else-template.component';
import {ReactiveFormsModule} from '@angular/forms';
import {RoleSentinelWithElseTemplateRoutingModule} from './role-sentinel-with-else-template-routing.module';
import {RoleService} from '../role.service';
import {RoleSentinelModule} from '@ui-tool/core';
import {ROLE_SENTINEL_SERVICE} from '@ui-tool/core';
import {RoleSentinelService} from '../role-sentinel.service';

@NgModule({
  declarations: [
    RoleSentinelWithElseTemplateComponent
  ],
  imports: [
    RoleSentinelWithElseTemplateRoutingModule,
    ReactiveFormsModule,
    RoleSentinelModule.forRoot({
      providers: [
        {
          provide: ROLE_SENTINEL_SERVICE,
          useClass: RoleSentinelService
        }
      ]
    })
  ],
  exports: [
    RoleSentinelWithElseTemplateComponent
  ],
  providers: [
    RoleService
  ]
})
export class RoleSentinelWithElseTemplateModule {

}
